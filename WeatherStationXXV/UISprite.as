package
{
	import flash.display.Sprite;
	
	import mx.core.UIComponent;

	public class UISprite extends UIComponent
	{
		public var sprite:Sprite;
		
		public function UISprite( /*s:Sprite = null*/ )
		{
			super();
			
			sprite = new Sprite();
			this.addChild( sprite );
			
		}
/*
		public function get sprite():Sprite
		{
			return sp;
			
		}
		
		public function set	sprite( s:Sprite ):void
		{
			if( sp != null )
			{
				this.removeChild( sp );
			}
			
			sp = s;
			this.addChild( sp );
			
		}
*/
	}
	
}