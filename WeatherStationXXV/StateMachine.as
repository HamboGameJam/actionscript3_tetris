package
{
	import IState;
	
	public class StateMachine
	{
	    private var currentState:IState;
	    private var previousState:IState;
	    private var nextState:IState;
	    
	    public function StateMachine()
	    {
	        currentState = null;
	        previousState = null;
	        nextState = null;
	    }
	    
	    public function GetCurrentState():IState
	    {
	    	return currentState;
	    }
	    
	    public function SetNextState( s:IState ):void
	    {
	        nextState = s;
	    }
	    
	    public function Update( time:Number ):void
	    {
	        if( currentState )
	        {
	            currentState.Update( time );
	        }
	    }
	    
	    public function ChangeState( s:IState ):void
	    {
	    	if( currentState != null )
	    	{
	        	currentState.Exit();	        	
	        	previousState = currentState;
	     	}
	        
	        currentState = s;	  
	        currentState.Enter();
	    }
	    
	    public function GoToPreviousState():void
	    {
	        ChangeState( previousState );
	    }
	    
	    public function GoToNextState():void
	    {
	        ChangeState( nextState );
	    }
	}
}