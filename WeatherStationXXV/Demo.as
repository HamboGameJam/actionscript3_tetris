// Holds the information read from each record
// of the XML file (demoreel.xml) that contains
// information of each of the projects involved
 
package
{
	import mx.controls.Image;
	
	public class Demo
	{
		public var strTitle:String;
		public var arrStrDescription:Array;
		public var arrArrStrTechSpec:Array;
		public var strThumbnailCaption:String;
		public var imageThumbnail:Image;
		
		// intDateSpan - Data structure - meaning of bits in int...
		//                           4 3 2 1 - Month Start    2^4   
		//                  12 ... 5         - Month Spanned  2^8          
		//  32 ... 15 14 13                  - Year  Start	 2^20 	
		private var intDateSpan:int;

		public function Demo()
		{
			arrArrStrTechSpec = new Array();
			intDateSpan = 0;
		}
			
		public function get DateString():String
		{
			var strDateString:String;
			
			var intFinalMonth:int = (( this.intMonthStart + this.intMonthsSpanned ) % 12);
			var intFinalYear:int  = (( this.intMonthStart + this.intMonthsSpanned ) / 12) + this.intYearStart;
			
			strDateString = "" + this.intMonthStart + "/" +
			                     this.intYearStart  + " - " +
			                     intFinalMonth      + "/" +
			                     intFinalYear;
			                     
			
			return strDateString;
		}
			
		///////// intMonthStart /////////
		public function get intMonthStart():int
		{			
			return (intDateSpan & 0xF);			
		}
		
		public function set intMonthStart( iMonthStart:int ):void
		{			
			intDateSpan = ( (iMonthStart             )| 
							(intDateSpan & 0xFFFFFFF0)  );			
		}
		///////// intMonthStart /////////

		
		///////// intMonthSpanned /////////
		public function get intMonthsSpanned():int
		{			
			return ( (intDateSpan & 0xFF0) >>> 4 );			
		}
		
		public function set intMonthsSpanned( iMonthSpanned:int ):void
		{			
			intDateSpan = ( (iMonthSpanned << 4       )| 
							(intDateSpan &  0xFFFFF00F)  );
		}
		///////// intMonthSpanned /////////
		
		
		///////// intYearStart /////////
		public function get intYearStart():int
		{			
			return ( intDateSpan >>> 12 );			
		}
		
		public function set intYearStart( iYearStart:int ):void
		{			
			intDateSpan = ( (iYearStart << 12   )| 
							(intDateSpan & 0xFFF)  );			
		}		
		///////// intYearStart /////////		
	}
}