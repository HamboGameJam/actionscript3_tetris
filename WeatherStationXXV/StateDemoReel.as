package
{
	import flash.display.LoaderInfo;
	import flash.events.*;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	
	import mx.containers.Canvas;
	import mx.containers.HBox;
	import mx.containers.VBox;
	import mx.controls.Button;
	import mx.controls.HRule;
	import mx.controls.Image;
	import mx.controls.Label;
	import mx.core.ScrollPolicy;
	
    public class StateDemoReel implements IState
    {  
    	private var arrDemoRecords:Array;
    	private var IsLoaded:Boolean;
    	private var booleanXMLFileIsLoaded:Boolean;
    	private var strLabelMiddleTop:String;
    	private var urlloaderXML:URLLoader;
    	private var urlrequestXML:URLRequest;    	
    	private var xmlDemoReelProjects:XML;
    	
    	private var intCurrentDemoIndex:int;
    	
    	//Complete Pannel
    	private var hboxDemoReel:HBox;
    	
    	//left panel
    	private var vboxDemoReelColumnLeft:VBox;
    	private var vboxDemoReelColumnLeftTop:VBox;
    	private var canvasDemoReelImage:Canvas;
    	//private var spriteDemoReelImage:Sprite;
    	private var labelDemoReelImageCaption:Label;
    	private var hboxDemoReelColumnLeftBottom:HBox;
    	private var buttonDemoReelNext:Button;
    	private var buttonDemoReelPrev:Button;
    	//private var imageNext:Image;
    	//private var imagePrev:Image;    	
    	
    	//right panel
    	private var vboxDemoReelColumnRight:VBox;
    	private var labelDemoReelTitle:Label;
    	private var labelDemoReelDate:Label;
    	private var hrule:HRule;
    	private var labelDemoReelDescription:Label;
    	private var vboxDemoReelTecspecs:VBox;
    	private var labelDemoReelTecspecs:Label;
    	
    	//private var windowWindow:Window;	 

    	public function StateDemoReel()
    	{
    		IsLoaded = false;
    		booleanXMLFileIsLoaded = false;
    		
    		urlloaderXML = new URLLoader();
			urlrequestXML = new URLRequest("assets/demoreel.xml");
			urlloaderXML.load(urlrequestXML);
			urlloaderXML.addEventListener(Event.COMPLETE, onXMLFileLoaded );
			
			hboxDemoReel = new HBox();
			hboxDemoReel.percentWidth = 100;
			hboxDemoReel.percentHeight = 100;


			//hboxDemoReel.setStyle("horizontal-Align", "right");
			
			
			
			//////
			canvasDemoReelImage = new Canvas();
			
			canvasDemoReelImage.width = 171;
			canvasDemoReelImage.minWidth = 171;
			canvasDemoReelImage.maxWidth = 171;
			canvasDemoReelImage.height = 171;
			canvasDemoReelImage.minHeight = 171;
			canvasDemoReelImage.maxHeight = 171;						
			canvasDemoReelImage.horizontalScrollPolicy = ScrollPolicy.OFF;
	    	canvasDemoReelImage.verticalScrollPolicy = ScrollPolicy.OFF;
	    	
	    	vboxDemoReelColumnLeft = new VBox();
	    	vboxDemoReelColumnLeft.setStyle("horizontal-Align", "center");
			vboxDemoReelColumnLeft.percentWidth  = 50;
			vboxDemoReelColumnLeft.percentHeight = 100;
			
			vboxDemoReelColumnRight = new VBox();
			vboxDemoReelColumnRight.percentWidth  = 50;
			vboxDemoReelColumnRight.percentHeight = 100;
			//////

			
			vboxDemoReelColumnLeftTop = new VBox();
			vboxDemoReelColumnLeftTop.percentWidth  = 100;
			vboxDemoReelColumnLeftTop.percentHeight =  80;
			vboxDemoReelColumnLeft.setStyle("vertical-Align", "middle");
			
			hboxDemoReelColumnLeftBottom = new HBox();
			hboxDemoReelColumnLeftBottom.percentWidth  = 90;
			hboxDemoReelColumnLeftBottom.percentHeight = 20;
			hboxDemoReelColumnLeftBottom.setStyle("horizontal-Align", "right");
			
			labelDemoReelImageCaption = new Label();
			labelDemoReelImageCaption.styleName = "tinyText";
			
			/*
			imageNext = new( Image );
			imageNext.load( "assets/next_icon_L.png" );
			imageNext.percentHeight = 90;
			imageNext.percentWidth  = 90;
			*/
			
			buttonDemoReelNext = new( Button );
	    	buttonDemoReelNext.percentWidth = 45;
	    	buttonDemoReelNext.height = 25;
	    	//buttonDemoReelNext.addChild( imageNext );
	    	buttonDemoReelNext.styleName = "mediumText";
	    	buttonDemoReelNext.label = ">";
	    	//buttonDemoReelNext.
	    	
	    	/*
	    	imagePrev = new( Image );
			imagePrev.load( "assets/prev_icon_L.png" );
			imagePrev.percentHeight = 90;
			imagePrev.percentWidth  = 90;	    	
	    	*/
	    	
	    	buttonDemoReelPrev = new( Button );
	    	buttonDemoReelPrev.percentWidth = 45;
	    	buttonDemoReelPrev.height = 25;
	    	//buttonDemoReelPrev.addChild( imagePrev );
	    	buttonDemoReelPrev.styleName = "mediumText";
	    	buttonDemoReelPrev.label = "<";
	    	
	    	labelDemoReelTitle = new Label();
			labelDemoReelTitle.styleName = "tinyText";
			
			labelDemoReelDate = new Label();
			labelDemoReelDate.styleName = "tinyText";
			
			hrule = new HRule();
			hrule.setStyle( "strokeWidth", "2" );
			hrule.setStyle( "color", "#FFFFFF" );
			hrule.percentWidth = 100;
			
			
			vboxDemoReelColumnLeftTop.addChild( canvasDemoReelImage );
			vboxDemoReelColumnLeftTop.addChild( labelDemoReelImageCaption );
			
			vboxDemoReelColumnLeft.addChild( vboxDemoReelColumnLeftTop );
			
			
						
			
			vboxDemoReelColumnLeft.addChild( hboxDemoReelColumnLeftBottom );
			hboxDemoReelColumnLeftBottom.addChild( buttonDemoReelPrev );
			hboxDemoReelColumnLeftBottom.addChild( buttonDemoReelNext );			
			
			buttonDemoReelPrev.addEventListener(MouseEvent.CLICK, onPrevButtonClick);
			buttonDemoReelNext.addEventListener(MouseEvent.CLICK, onNextButtonClick);
			
			hboxDemoReel.addChild( vboxDemoReelColumnLeft );
			
			
			vboxDemoReelColumnRight.addChild( labelDemoReelTitle );
			vboxDemoReelColumnRight.addChild( labelDemoReelDate  );
			
 	   		hboxDemoReel.addChild( vboxDemoReelColumnRight );
 	   		
 	   		intCurrentDemoIndex = 0;

    	}

    	
    	private function onXMLFileLoaded( evnt:Event ):void
    	{
    		var ldr:URLLoader = evnt.target as URLLoader;
    		
    		if (ldr != null)
    		{
    			var arrayPlaceHolder:Array;
    			var demoPlaceholder:Demo;
    			var labelPlaceholder:Label;

    			
    			xmlDemoReelProjects = new XML(urlloaderXML.data);
	    		arrDemoRecords = new Array();
	    		
	    		for( var intDemoRecordIndex:int = 0;
	    		         intDemoRecordIndex < xmlDemoReelProjects.child("demo").length();
	    		         intDemoRecordIndex ++                                                          )
	    		{
	    			demoPlaceholder = new Demo();
	    			
	    			demoPlaceholder.strTitle            = xmlDemoReelProjects.child("demo")[intDemoRecordIndex].title;
	    			demoPlaceholder.intMonthStart       = xmlDemoReelProjects.child("demo")[intDemoRecordIndex].month_start;
	    			demoPlaceholder.intYearStart        = xmlDemoReelProjects.child("demo")[intDemoRecordIndex].year_start;
	    			demoPlaceholder.intMonthsSpanned    = xmlDemoReelProjects.child("demo")[intDemoRecordIndex].month_span;
	    			demoPlaceholder.strThumbnailCaption = xmlDemoReelProjects.child("demo")[intDemoRecordIndex].caption;
	    			
	    			
	    			//for every techstep
	    			for( var intTechSpecIndex:int = 0;
	    					 intTechSpecIndex < xmlDemoReelProjects.child("demo")[intDemoRecordIndex].techspec.length();
	    					 intTechSpecIndex ++                                                                         )
	    			{
						arrayPlaceHolder = Utilities.GetLabelsFromString( xmlDemoReelProjects.child("demo")[intDemoRecordIndex].techspec[intTechSpecIndex], 32 );
						
						//for every label of each techstep
						for( var intCurrentSpecLine:int = 0;
								 intCurrentSpecLine < arrayPlaceHolder.length;
								 intCurrentSpecLine ++                         )
						{
							labelPlaceholder = ( arrayPlaceHolder[intCurrentSpecLine] as Label );
							
							if( intCurrentSpecLine == 0 )
							{
								labelPlaceholder.text = "- " + labelPlaceholder.text;
							}
							else
							{
								labelPlaceholder.text = "   " + labelPlaceholder.text;
							}
							
							labelPlaceholder.styleName = "microText";									
							SharedData.Instance.addShadow( labelPlaceholder );
						}
						
						demoPlaceholder.arrArrStrTechSpec.push( arrayPlaceHolder );
						
						
	    			}
	    			
	    			demoPlaceholder.imageThumbnail = new Image();	    			
	    			demoPlaceholder.imageThumbnail.load( xmlDemoReelProjects.child("demo")[intDemoRecordIndex].thumbnail.toString() );

					demoPlaceholder.arrStrDescription = Utilities.GetLabelsFromString( xmlDemoReelProjects.child("demo")[intDemoRecordIndex].description, 32 );
					
					for( var iDescriptionIndex:int = 0;
							 iDescriptionIndex < demoPlaceholder.arrStrDescription.length;
							 iDescriptionIndex ++                                          )
					{
						labelPlaceholder = (demoPlaceholder.arrStrDescription[ iDescriptionIndex ] as Label);
						labelPlaceholder.styleName = "microText";									
						SharedData.Instance.addShadow( labelPlaceholder );
					}
	    			
	    			arrDemoRecords.push( demoPlaceholder );
	    		}
	    		
	    		booleanXMLFileIsLoaded = true;
	    	}
	    	else
	    	{
	    		// error loading url file
	    		strLabelMiddleTop = "Error loading demo reel xml source file!"; 
	    	}
    	}
    	
    	private function CheckIfLoadingComplete():Boolean
		{
			var imgPlaceholder:Image;
			var loaderinfo:LoaderInfo;
			
			if( booleanXMLFileIsLoaded )
			{
				strLabelMiddleTop = "Demo Reel";
				IsLoaded = true;
				
			}			
						
			return IsLoaded;
			
		}
		
    	public function Enter():void
    	{
    		SharedData.Instance.labelMiddletop.text = strLabelMiddleTop;
    		
    		SharedData.Instance.labelMenu0.text = "Main Menu";
    		SharedData.Instance.labelMenu1.text = "";
    		SharedData.Instance.labelMenu2.text = "";
    		SharedData.Instance.labelMenu3.text = "";
 	   		SharedData.Instance.labelMenu4.text = "Quit";
 	   		
 	   		SharedData.Instance.labelMenu0.addEventListener( MouseEvent.CLICK, SharedData.Instance.onClickMainMenu );
 	   		SharedData.Instance.labelMenu4.addEventListener( MouseEvent.CLICK, SharedData.Instance.onClickExit );

 	   		SharedData.Instance.hboxMainpannel.addChild( hboxDemoReel );

    	}


    	


		//intDemoRecordIndex
		//xmlDemoReelProjects.child("demo").length();
    	private function onPrevButtonClick( mevent:MouseEvent ):void
    	{
    		if( intCurrentDemoIndex == 0 )
    		{
    			intCurrentDemoIndex = ( xmlDemoReelProjects.child("demo").length() - 1 );
    		}
    		else
    		{
    			intCurrentDemoIndex -= 1;
    		}
    		
    		RefreshData();
    	}
    	
    	private function onNextButtonClick( mevent:MouseEvent ):void
    	{
    		if( intCurrentDemoIndex == ( xmlDemoReelProjects.child("demo").length() - 1 ) )
    		{
    			intCurrentDemoIndex = 0;
    		}
    		else
    		{
    			intCurrentDemoIndex += 1;
    		}
    		
    		RefreshData();
    	}
    	
    	
    	public function addChildren():void
    	{


    	}

		public function removeChildren():void
		{

		}

    	public function Update( time:Number ):void
    	{
			if( IsLoaded )
    		{
    			
    			/*
    			if( SharedData.Instance.IsLoaded )
		    	{
		    		SharedData.Instance.GameStateMachine.ChangeState( SharedData.Instance.stateTitle );
		    		
		    	}
		    	else
		    	{
		    		SharedData.Instance.CheckIfLoadingComplete();
		    			    			
		    		if( SharedData.Instance.IsLoaded )
		    		{
		    			 SharedData.Instance.OnLoadingComplete();
		    			 
		    		}
		    		
		    		m_avm1movieLoadingBarFront.width = m_avm1movieLoadingBar.width - (SharedData.Instance.LoadingPercentage * m_avm1movieLoadingBar.width);
		    		m_avm1movieLoadingBarFront.x     = m_avm1movieLoadingBar.x + ( m_avm1movieLoadingBar.width - m_avm1movieLoadingBarFront.width );
		    	}
    		    */
    		}
    		else
    		{
    			strLabelMiddleTop = "Loading Demo Reel Data...";
    			
    			if( CheckIfLoadingComplete() )
    			{	
	 	   			var arrayPlaceholder:Array;
    				var demoPlaceholder:Demo;
    				var labelPlaceholder:Label;
    				
    				var iIndex:int;
    				
    				SharedData.Instance.addShadow( canvasDemoReelImage );

	    			SharedData.Instance.addShadow( buttonDemoReelPrev );
	    			SharedData.Instance.addShadow( buttonDemoReelNext );
	    			SharedData.Instance.addShadow( hrule );
	    			SharedData.Instance.addShadow( labelDemoReelImageCaption );
	    			SharedData.Instance.addShadow( labelDemoReelDate );
	    			SharedData.Instance.addShadow( labelDemoReelTitle );
					
    				RefreshData();
    				
    				vboxDemoReelColumnRight.addChild( hrule );
    				 	   			
	 	   			for( iIndex = 0;
	 	   				 iIndex < (arrDemoRecords[ intCurrentDemoIndex ] as Demo).arrStrDescription.length;
	 	   				 iIndex ++                                                                          )
	 	   			{
	 	   				vboxDemoReelColumnRight.addChild( (arrDemoRecords[ intCurrentDemoIndex ] as Demo).arrStrDescription[iIndex] );
	 	   			}

    				demoPlaceholder = (arrDemoRecords[intCurrentDemoIndex] as Demo);
	 	   			
	 	   			for( iIndex = 0;
	 	   				 iIndex < demoPlaceholder.arrArrStrTechSpec.length;
	 	   				 iIndex ++                                                                          )
	 	   			{
	 	   				arrayPlaceholder = demoPlaceholder.arrArrStrTechSpec[iIndex];
	 	   				//vboxDemoReelColumnRight.addChild( arrayPlaceholder[iIndex] );
	 	   				
	 	   				for( var iLabelIndex:int = 0;
	 	   						 iLabelIndex     < arrayPlaceholder.length;
	 	   						 iLabelIndex ++                             )
	 	   				{
	 	   					labelPlaceholder = ( arrayPlaceholder[iLabelIndex] as Label );
	 	   					vboxDemoReelColumnRight.addChild( labelPlaceholder );
	 	   				}
	 	   			}
	 	   			
	 	   			/*
    				for( iIndex = 0;
	 	   				 iIndex < (arrDemoRecords[intCurrentDemoIndex] as Demo).arrArrStrTechSpec.length;
	 	   				 iIndex ++                                                                          )
	 	   			{
	 	   				vboxDemoReelColumnRight.addChild( (arrDemoRecords[intCurrentDemoIndex] as Demo).arrArrStrTechSpec[iIndex] );
	 	   			}
	 	   			
	 	   			
    				
    				addChildren();
    				Initialize();
    				 */    				
    			}
    			
    		}
    		
    		SharedData.Instance.labelMiddletop.text = strLabelMiddleTop;
    	}
    	
    	private function RefreshData():void
    	{
			
    		var imageThumb:Image = ( arrDemoRecords[ intCurrentDemoIndex ] as Demo ).imageThumbnail;
    		imageThumb.percentWidth  = 90;
    		imageThumb.percentHeight = 90;
    		canvasDemoReelImage.removeAllChildren();
    		canvasDemoReelImage.addChild( imageThumb );
    		
    		labelDemoReelImageCaption.text = ( arrDemoRecords[ intCurrentDemoIndex ] as Demo ).strThumbnailCaption;
    		labelDemoReelTitle.text		   = ( arrDemoRecords[ intCurrentDemoIndex ] as Demo ).strTitle;
    		labelDemoReelDate.text         = ( arrDemoRecords[ intCurrentDemoIndex ] as Demo ).DateString;
    		
    		//TODO TODO TODO
    		// description to description - line
    		//labelDemoReelDescription.text  = ( arrDemoRecords[ intCurrentDemoIndex ] as Demo ).strDescription;
    		/*
    		labelDemoReelTitle.text		   = ( arrDemoRecords[ intCurrentDemoIndex ] as Demo ).strTitle;
    		labelDemoReelDate.text         = ( arrDemoRecords[ intCurrentDemoIndex ] as Demo ).DateString;
    		
    		
    		for( var iTechspecIndex:int = 0;
    		         iTechspecIndex < ( arrDemoRecords[ intCurrentDemoIndex ] as Demo ).arrStrTechSpec.length;
    		         iTechspecIndex ++                                                                         )
    		{
    			if( iTechspecIndex == 0 )
    			{
    				labelDemoReelTecspecs.text = "*" + ( arrDemoRecords[ intCurrentDemoIndex ] as Demo ).arrStrTechSpec[ iTechspecIndex ] as String;
    			}
    			else
    			{
    				labelDemoReelTecspecs.text = labelDemoReelTecspecs.text + "\n" + ( ( arrDemoRecords[ intCurrentDemoIndex ] as Demo ).arrStrTechSpec[ iTechspecIndex ] as String );
    			}
    				
    		}
    		
    		//labelDemoReelTecspecs.text	   = ( arrDemoRecords[ intCurrentDemoIndex ] as Demo ).strTitle;
    		/**/
    	}

    	public function Exit():void
    	{
    		SharedData.Instance.hboxMainpannel.removeChild( hboxDemoReel );
    		SharedData.Instance.labelMiddletop.text = "";
    		
    		SharedData.Instance.labelMenu0.removeEventListener( MouseEvent.CLICK, SharedData.Instance.onClickMainMenu );
    		SharedData.Instance.labelMenu4.removeEventListener( MouseEvent.CLICK, SharedData.Instance.onClickExit );
    		
    		/*
    		if( IsLoaded == true )
 	   		{
 	   			var iIndex:int;
 	   			
 	   			for( iIndex = 0;
 	   				 iIndex < (arrDemoRecords[ intCurrentDemoIndex ] as Demo).arrStrDescription.length;
 	   				 iIndex ++                                                                          )
 	   			{
 	   				vboxDemoReelColumnRight.removeChild( (arrDemoRecords[ intCurrentDemoIndex ] as Demo).arrStrDescription[iIndex] );
 	   			}
 	   			
 	   		}
 	   		*/
    	}    	
    	//Event Complete && Progress
    	//Complete
    	
    	//PRogress
    	//progress (ProgressEvent.PROGRESS)

    }
}