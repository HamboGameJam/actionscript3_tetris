package
{
	import flash.display.Sprite;
	import flash.display.Loader;
	import flash.events.Event;
	import flash.events.ProgressEvent;
	import flash.net.URLRequest;

	public class ImageSprite extends Sprite
	{
		public  var IsLoaded:Boolean;
		private var strFileName:String;
		private var loader:Loader;
		
		public var nLoadedData:Number;
		public var nTotalData:Number;
		
		public function ImageSprite( stringFileName:String )
		{
			//super();
			
			IsLoaded = false;
			strFileName = stringFileName;
			createLoader();
			load(new URLRequest(stringFileName));
		}
		
		private function createLoader():void
		{
			loader = new Loader();
			
			loader.contentLoaderInfo.addEventListener( Event.COMPLETE, completeListener );
			loader.contentLoaderInfo.addEventListener( Event.INIT, initListener );
			loader.contentLoaderInfo.addEventListener( ProgressEvent.PROGRESS, progressListener );
			
		}
		
		private function load (urlRequest:URLRequest):void
		{
			loader.load(urlRequest);
			
		}
		
		private function initListener(e:Event):void
		{			
			addChild(loader.content);

		}
		
		private function completeListener(e:Event):void
		{
			IsLoaded = true;

		}
		
		private function progressListener(pe:ProgressEvent):void
		{
			nTotalData  = pe.bytesTotal;
			nLoadedData = pe.bytesLoaded;
			
		}
		
	}
}