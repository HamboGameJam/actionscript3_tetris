package
{
	import flash.display.NativeWindowInitOptions;
	import flash.events.*;
	import flash.geom.Rectangle;
	import flash.html.HTMLLoader;
	import flash.html.HTMLPDFCapability;
	import flash.net.URLRequest;
	
	import mx.controls.Label;		
	
    public class StateMainMenu implements IState
    {    	
    	//resume
    	private var booleanPDFCapable:Boolean;
    	private var booleanPDFDisplayed:Boolean;
    	private var htmlloaderPDF:HTMLLoader;
    	private var urlrequestPDFResume:URLRequest;
	    private var nativewindowinitoptions:NativeWindowInitOptions;
	    private var rectBounds:Rectangle;
	    private var labelBadReader:Label;
	    //resume
	    
	    //demoReel
	    
	    //demoReel
	    
    	public function StateMainMenu()
    	{
    		//resume
    		
    		booleanPDFCapable   = false;
    		booleanPDFDisplayed = false;
    		
    		if( HTMLLoader.pdfCapability ==
    		    HTMLPDFCapability.STATUS_OK )
    		{	
    			booleanPDFCapable = true;
    			nativewindowinitoptions = new NativeWindowInitOptions();
    			rectBounds = new Rectangle(10, 10, 600, 400);
    			urlrequestPDFResume = new URLRequest("assets/NoelBenitez_Resume.pdf");
    			
    			
    			/*
    			labelBadReader = new Label();
    			labelBadReader.styleName = "tinyText";
    			labelBadReader.setStyle( "color", "0xffffff" );
    			labelBadReader.height = 60;
    			labelBadReader.text = "Update to Adobe Reader 8.0 \n or better to view Resumé!";
    			*/////////////////////
    		}
    		else
    		{
    			SharedData.Instance.labelMiddletop.text = "Update to Adobe Reader 8.0 or better\n to view Resumé!";
    		}
    		
    		//resume
    	}  	

    	public function Enter():void
    	{
    		SharedData.Instance.labelMenu0.text = "Demo Reel";
    		SharedData.Instance.labelMenu1.text = "Resumé";
    		SharedData.Instance.labelMenu2.text = "Play Games";
    		SharedData.Instance.labelMenu3.text = "Send e-Mail";
 	   		SharedData.Instance.labelMenu4.text = "Quit";
 	   		
 	   		SharedData.Instance.labelMenu0.addEventListener(MouseEvent.CLICK, onClickDemoReel                 );
 	   		SharedData.Instance.labelMenu1.addEventListener(MouseEvent.CLICK, onClickResume                   );
			SharedData.Instance.labelMenu2.addEventListener(MouseEvent.CLICK, onClickPlayGames                );
 	   		SharedData.Instance.labelMenu4.addEventListener(MouseEvent.CLICK, SharedData.Instance.onClickExit );
 	   		
    	}
    	
    	private function onPDFClosing(event:Event):void
    	{
    		booleanPDFDisplayed =  false;
    		
    	}
    	
    	private function onClickDemoReel( mevent:MouseEvent ):void
    	{
    		SharedData.Instance.statemachine.ChangeState( SharedData.Instance.stateDemoReel );
    		
    	}
    	
    	private function onClickResume( mevent:MouseEvent ):void
    	{    		
    		if( booleanPDFDisplayed == false )
    		{
	    		booleanPDFDisplayed = true;
	    		
	    		if( booleanPDFCapable == true )
	    		{			
	 				htmlloaderPDF = HTMLLoader.createRootWindow(true, nativewindowinitoptions, true, rectBounds);
	 	    		htmlloaderPDF.load( urlrequestPDFResume );
	 	    		htmlloaderPDF.stage.nativeWindow.title = "Noel Benítez Cruz - The CV...";

 	    		    htmlloaderPDF.stage.nativeWindow.addEventListener( Event.CLOSING, onPDFClosing ); 
	
	 	    	}
	 	    	
	 	    	else
	 	    	{
	 	    		SharedData.Instance.labelMiddletop.text = "You need Adobe Reader 8.0 or better to view Resumé";

	 	    	}
	 	    	
			}
			else
			{
				booleanPDFDisplayed = false;
				
				if( booleanPDFCapable == true )
	    		{
	    			htmlloaderPDF.stage.nativeWindow.close();
	    		}

			}
	   	}
	   	
    	private function onClickPlayGames( mevent:MouseEvent ):void
    	{
    		SharedData.Instance.statemachine.ChangeState( SharedData.Instance.stateGameTetris );
    		
    	}	   	
    	
    	public function Exit():void
    	{
    		SharedData.Instance.labelMenu0.removeEventListener( MouseEvent.CLICK, onClickDemoReel                 );
    		SharedData.Instance.labelMenu1.removeEventListener( MouseEvent.CLICK, onClickResume                   );
    		SharedData.Instance.labelMenu4.removeEventListener( MouseEvent.CLICK, SharedData.Instance.onClickExit );
    		
    	}
    	
    	public function addChildren():void
    	{


    	}
    	
		public function removeChildren():void
		{

		}  
    	
    	public function Update( time:Number ):void
    	{

    	}
    	
    }
}
