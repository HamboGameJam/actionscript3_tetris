package 
{	
	public interface IState
	{	    
	 	function Enter():void;
	 	function Exit():void;
		function Update( time:Number ):void;
		function addChildren():void;
		function removeChildren():void;
	}
}