package
{
	import flash.text.*;
	import flash.utils.ByteArray;
	
	import mx.controls.Label;
		
	public class Utilities
    {
    	/*
		static public function NewTextField(text:String, tf:TextFormat, s:Sprite, x:Number, y:Number, width:Number):TextField
		{
			var tField:TextField = new TextField();
			
			tField.x = x;
			tField.y = y;
			tField.width = width;
			tField.defaultTextFormat = tf;
			tField.selectable = false;
			tField.multiline = true;
			tField.wordWrap = true;
			if (tf.align == "left") {
				tField.autoSize = TextFieldAutoSize.LEFT;
			} else {
				tField.autoSize = TextFieldAutoSize.CENTER;
			}
			tField.text = text;
			s.addChild(tField);
			
			return tField;
		}
		*/
		
		static public function NewTextField(text:String, tf:TextFormat, x:Number, y:Number, width:Number):TextField
		{
			var tField:TextField = new TextField();
			
			tField.x = x;
			tField.y = y;
			tField.width = width;
			tField.defaultTextFormat = tf;
			tField.selectable = false;
			tField.multiline = true;
			tField.wordWrap = true;
			if (tf.align == "left") {
				tField.autoSize = TextFieldAutoSize.LEFT;
			} else {
				tField.autoSize = TextFieldAutoSize.CENTER;
			}
			tField.text = text;
			
			return tField;
			
		}
		
		static public function FillWithCharacter( strCharacter:String, uiNumberOfCharacters:uint ):String
		{
			var strReturn:String = "";
			
			while( uiNumberOfCharacters > 0 )
			{
				strReturn = strReturn+strCharacter;
				uiNumberOfCharacters -= 1;
			}			
			
			return strReturn;
			
		}
		
		static public function Clone(source:Object):*
		{
		    var myBA:ByteArray = new ByteArray();
		    myBA.writeObject(source);
		    myBA.position = 0;
		    
		    return(myBA.readObject());
		    
		}
		
		static public function GetLabelsFromString( strSource:String, iLabelLength:int ):Array
		{
			var arrLabels:Array;
			var bIterationsLeft:Boolean;
			var intCurrentCharacterIndex:int;
			var intEndingCharacterIndex:int;
			var label:Label;
			var strRemainder:String;
			
			arrLabels = new Array();
			bIterationsLeft = true;
			strRemainder = strSource;
			
			do
			{
				if( strRemainder.length > iLabelLength )
				{
					intCurrentCharacterIndex = iLabelLength;				
					
					while( strRemainder.charAt(intCurrentCharacterIndex) != " "  )
					{
						if( intCurrentCharacterIndex > 1 )
						{
							intCurrentCharacterIndex -= 1;
						}
						else
						{
							break;
						}
						
					}
					
					label = new Label();
					label.text = strRemainder.substr( 0, intCurrentCharacterIndex );
					
					arrLabels.push( label );
					
					
					
					//will this IF be ever hit?
					if(  ( intCurrentCharacterIndex + 1 ) >=
					     ( strRemainder.length          )     )
					{
						bIterationsLeft = false;
						
					}
					
					strRemainder = strRemainder.substr( intCurrentCharacterIndex+1, strRemainder.length );
					
					// "0000 00000          0000    0000  000  "
					// "          000  00000 0000 00000          0000    0000  000  "
					// "blah blhaaah blah blah blah  Blahh      blahh           blah"
					// ".........^.........^.........^.........^.........^.........^			
				}
				else
				{
					label = new Label();
					label.text = strRemainder;
					arrLabels.push( label );

					bIterationsLeft = false;
					
				}
			}while( bIterationsLeft );
			
			return arrLabels;

		}
		
		static public function MergeColors( iColor1:int, iColor2:int, number2ndColorPercent:Number ):int
		{			
			var intDeltaR:int;
            var intDeltaG:int;
            var intDeltaB:int;
            
            var intNewR:int;
            var intNewG:int;
            var intNewB:int;
            
            var iColorReturn:int;
            
			if( number2ndColorPercent < 0.00 )
			{
				number2ndColorPercent = 0.00;
			}
			else if( number2ndColorPercent > 1.00 )
			{
				number2ndColorPercent = 1.00;							
			}
			
			intDeltaR = ( (iColor2 >> 16) & 0xFF ) - ( (iColor1 >> 16) & 0xFF );
            intDeltaG = ( (iColor2 >> 8 ) & 0xFF ) - ( (iColor1 >> 8 ) & 0xFF );
            intDeltaB = ( (iColor2      ) & 0xFF ) - ( (iColor1      ) & 0xFF );
                	
            intNewR   = (  ( (iColor1 >> 16) & 0xFF )  ) + Math.floor( intDeltaR * number2ndColorPercent );
            intNewG   = (  ( (iColor1 >> 8 ) & 0xFF )  ) + Math.floor( intDeltaG * number2ndColorPercent );
            intNewB   = (  ( (iColor1      ) & 0xFF )  ) + Math.floor( intDeltaB * number2ndColorPercent );
                	
            iColorReturn = (  ( (intNewR << 16) & 0xFF0000 ) |
                			  ( (intNewG << 08) & 0x00FF00 ) | 
                			  ( (intNewB      ) & 0x0000FF )    ); 
			
			return iColorReturn;
		
		}
		
		static public function GenerateRandomColor():int
        {
        	var intRandomShade:int = Math.round(Math.random()*0xffffff);                
                        
            return (  Math.min(0xFFFFFF, intRandomShade) );
        }
		
		static public function IntToHex(i:int):String
        {
        	var iNibble:int;
        	var iPlaceholder:int = i;
        	var stringHex:String = '';
        	var stringArr:String = 'FEDCBA';
        	
        	for(var intNibbleIndex:int;
        	        intNibbleIndex < 8;
        	        intNibbleIndex++    )
        	{
        		iNibble = 0xF & iPlaceholder;
        		
        		if(iNibble > 9)
        		{
         			stringHex = ( stringArr.substr((9-iNibble), 1) ) +
        						( stringHex                         );
        		}
        		else
        		{
        			stringHex = ( iNibble.toString() ) +
        						( stringHex          );
        		}
        		
        		iPlaceholder = iPlaceholder >> 4;
        	}
        	
        	stringHex = "0x" + stringHex;
        	
        	
        	return stringHex;
        	
        }
        
    }
}