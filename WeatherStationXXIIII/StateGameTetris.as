package 
{
	import flash.display.*;
	import flash.events.*;
	
	import tetris.*;
	

	
    public class StateGameTetris implements IState
    {  

		private var uiSprite:UISprite;
		private var tet:SpriteGameTetris;
		
    	public function StateGameTetris()
    	{
    		//aCanvas = new Canvas();
			//aCanvas.width = 200;
    		//aCanvas.height = 200;
			tet = new SpriteGameTetris();

    		tet.Initialize(); // TODO 
    		uiSprite = new UISprite( );
    		uiSprite.width = uiSprite.width;
    		uiSprite.height = uiSprite.height;
    		uiSprite.sprite.addChild( tet );
    		uiSprite.sprite.width = uiSprite.sprite.width;
    		uiSprite.sprite.height = uiSprite.sprite.height;  
    		uiSprite.width = uiSprite.sprite.width;
    		uiSprite.height = uiSprite.sprite.height;
    		uiSprite.alpha= 1.0;
    		//uiSprite.addChild(
    		//this.addChildren( uiSprite );
    		
    	}   
		
    	public function Enter():void
    	{
    		SharedData.Instance.labelMiddletop.text = "Tetris";
    		
    		SharedData.Instance.labelMenu0.text = "Main Menu";
    		SharedData.Instance.labelMenu1.text = "";
    		SharedData.Instance.labelMenu2.text = "";
    		SharedData.Instance.labelMenu3.text = "";
 	   		SharedData.Instance.labelMenu4.text = "Quit";
 	   		
 	   		SharedData.Instance.labelMenu0.addEventListener( MouseEvent.CLICK, SharedData.Instance.onClickMainMenu );
 	   		SharedData.Instance.labelMenu4.addEventListener( MouseEvent.CLICK, SharedData.Instance.onClickExit );

 	   		SharedData.Instance.hboxMainpannel.addChild( uiSprite );
			uiSprite.sprite.graphics.beginFill(0xffffff );

    		//aCanvas.setStyle( "backgroundColor", Utilities.IntToHex(0xFFFFFF) );

    	}


    	
    	public function addChildren():void
    	{


    	}

		public function removeChildren():void
		{

		}

    	public function Update( time:Number ):void
    	{
			tet.Color = SharedData.Instance.intBGColorCurr;
    	}
    	
  
    	public function Exit():void
    	{
			SharedData.Instance.hboxMainpannel.removeChild( uiSprite );
			
    	}    	
    	/**/

    }
}

