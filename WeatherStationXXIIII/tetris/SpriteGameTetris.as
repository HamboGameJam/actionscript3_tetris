package tetris
{
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.geom.Rectangle;
	import flash.utils.*;

	public class SpriteGameTetris extends Sprite
	{
		public var Color:int;	
		
		private var numberTimeStart:Number;
		private var numberTimeElapsed:Number;
		
		private var block:Block;
		private var shapeBackground:Shape;				
		
		
		public function SpriteGameTetris( iWidth:int = 200, iHeight:int = 200 )
		{
			super();
			
			shapeBackground = new Shape();
			shapeBackground.graphics.moveTo( 0     , 0       );
			shapeBackground.graphics.lineTo( 0     , iHeight );
			shapeBackground.graphics.lineTo( iWidth, iHeight );	
			shapeBackground.graphics.lineTo( iWidth, 0       );
			shapeBackground.graphics.lineTo( 0     , 0       );
			this.addChild( shapeBackground );
			
			var singletonSharedDataGameTetris:SharedDataGameTetris = SharedDataGameTetris.Instance;

			Block.spriteGameTetris = this;
			
			singletonSharedDataGameTetris.iAppWidth = iWidth;
			singletonSharedDataGameTetris.iAppHeight = iHeight;
			singletonSharedDataGameTetris.iBlockWidth = (iWidth / 20);
			singletonSharedDataGameTetris.iBlockHeight = (iHeight / 20);
			singletonSharedDataGameTetris.spriteGameTetris = this;
			singletonSharedDataGameTetris.Initialize();
			
			addEventListener( Event.ENTER_FRAME, OnEnterFrame );
			
			numberTimeStart = getTimer();		

		}
		
		public function Initialize():void
		{	
			SharedDataGameTetris.Instance.stateSpriteGameTetrisMainMenu.Render();

		}
			
		private function Update( nElapsedTime:Number ):void
		{
			SharedDataGameTetris.Instance.stateMachine.Update( nElapsedTime );
			
		}
		
		private function OnEnterFrame( event:Event ):void
		{
			numberTimeElapsed = getTimer() - numberTimeStart;
			numberTimeStart = getTimer();
			
			Update( numberTimeElapsed );

		}
		
	}
	
	
}

import flash.geom.Point;
import flash.net.URLLoader;
import flash.net.URLRequest;
class SharedDataGameTetris
{
	public const QTY_ROWS:uint        = 20;
	public const QTY_COLUMNS:uint     = 10;
	public const QTY_TETRADTYPES:uint =  7;
		
	
	public var arrayTetradTypes:Array;
	public var blockWritter:BlockWritter;		
	public var dropShadowFilter:DropShadowFilter;
	public var glowFilter:GlowFilter;
	public var iAppWidth:int;
	public var iAppHeight:int;
	public var iBlockWidth:int;
	public var iBlockHeight:int;
	
	public var spriteBackToMainMenu:Sprite;
	public var spriteTextFieldsHighScores:Sprite;
	public var spriteGameTetris:SpriteGameTetris;
	public var textFormatBlack:TextFormat;
	public var textFormatWhite:TextFormat;
	
 	public var bIsXMLFileLoaded:Boolean;
	public var urlloaderXML:URLLoader;
    public var urlrequestXML:URLRequest;    	
    public var xmlHighScores:XML;
	
	public var stateMachine:StateMachine;
	public var stateGameTetrisInitialize         : StateGameTetrisInitialize;
	public var stateSpriteGameTetrisMainMenu     : StateSpriteGameTetrisMainMenu;
	public var stateSpriteGameTetris1PlayerLoop  : StateSpriteGameTetris1PlayerLoop;
	public var stateSpriteGameTetris2PlayersLoop : StateSpriteGameTetris2PlayersLoop;
	public var stateSpriteGameTetrisHighScore    : StateSpriteGameTetrisHighScore;
	
	static private var instance:SharedDataGameTetris = null;
	
	public function SharedDataGameTetris()
	{			
		stateMachine                     = new StateMachine();
		blockWritter                     = new BlockWritter();
				
		dropShadowFilter                 = new DropShadowFilter();
		dropShadowFilter.color = 0x000000;
        dropShadowFilter.alpha = 0.4;
        dropShadowFilter.blurX = 5;
        dropShadowFilter.blurY = 5;
        dropShadowFilter.distance = 5;
         
		glowFilter                       = new GlowFilter();
		glowFilter.color = 0xffaa00;
		glowFilter.alpha = 0.4;
		glowFilter.blurX = 5;
		glowFilter.blurY = 5;
		
		textFormatWhite                  = new TextFormat();
		textFormatWhite.align = "center";
		textFormatWhite.bold  = false;
		textFormatWhite.color = 0xffffff;
		
		textFormatBlack                  = new TextFormat();
		textFormatBlack.align = "center";
		textFormatBlack.bold  = true;
		textFormatBlack.color = 0x000000;
		
		var tetradType_1:TetradType;
		var tetradType_2:TetradType;
		var tetradType_3:TetradType;
		var tetradType_4:TetradType;
		var tetradType_5:TetradType;
		var tetradType_6:TetradType;
		var tetradType_7:TetradType;
		var point2:Point;
		var point3:Point;
		var point4:Point;
		
		
		point2 = new Point( -1, 0 );
		point3 = new Point( -1, 1 );
		point4 = new Point(  0, 1 );
		tetradType_1 = new TetradType( point2,point3,point4,
									   TetradType.FunctionRotateCW2by2Block,
									   TetradType.FunctionRotateCC2by2Block  );
		
		point2 = new Point( -1, 0 );
		point3 = new Point( -1, 1 );
		point4 = new Point(  1, 0 );									
		tetradType_2 = new TetradType( point2,point3,point4,
									   TetradType.FunctionRotateCW,
									   TetradType.FunctionRotateCC );
							
		point2 = new Point( -1, 0 );
		point3 = new Point(  1, 0 );
		point4 = new Point(  1, 1 );
		tetradType_3 = new TetradType( point2,point3,point4,
									   TetradType.FunctionRotateCW,
									   TetradType.FunctionRotateCC );

		point2 = new Point(  0, 1 );
		point3 = new Point( -1, 1 );
		point4 = new Point(  1, 0 );
		tetradType_4 = new TetradType( point2,point3,point4,
									   TetradType.FunctionRotateCW,
									   TetradType.FunctionRotateCC );
		
		point2 = new Point(  0, 1 );
		point3 = new Point(  1, 1 );
		point4 = new Point( -1, 0 );							
		tetradType_5 = new TetradType( point2,point3,point4,
									   TetradType.FunctionRotateCW,
									   TetradType.FunctionRotateCC );
		
		point2 = new Point(  0, 1 );
		point3 = new Point( -1, 0 );
		point4 = new Point(  1, 0 );									
		tetradType_6 = new TetradType( point2,point3,point4,
									   TetradType.FunctionRotateCW,
									   TetradType.FunctionRotateCC );										
		
		point2 = new Point( -1, 0 );
		point3 = new Point(  1, 0 );
		point4 = new Point(  2, 0 );							
		tetradType_7 = new TetradType( point2,point3,point4,
									   TetradType.FunctionRotateCW,
									   TetradType.FunctionRotateCC );										

		arrayTetradTypes = new Array();
		arrayTetradTypes.push( tetradType_1 );
		arrayTetradTypes.push( tetradType_2 );
		arrayTetradTypes.push( tetradType_3 );
		arrayTetradTypes.push( tetradType_4 );
		arrayTetradTypes.push( tetradType_5 );
		arrayTetradTypes.push( tetradType_6 );
		arrayTetradTypes.push( tetradType_7 );
		
		bIsXMLFileLoaded = false;
		urlloaderXML  = new URLLoader();
		urlrequestXML = new URLRequest("tetris/GameTetrisScores.xml");
		
		spriteTextFieldsHighScores    = new Sprite();
		spriteBackToMainMenu          = new Sprite();

		stateGameTetrisInitialize        = new StateGameTetrisInitialize();
		stateSpriteGameTetrisMainMenu    = new StateSpriteGameTetrisMainMenu();
		stateSpriteGameTetris1PlayerLoop = new StateSpriteGameTetris1PlayerLoop();
		stateSpriteGameTetrisHighScore   = new StateSpriteGameTetrisHighScore();
		
		stateMachine.ChangeState( stateGameTetrisInitialize );		

	}
	
	public function Initialize():void
	{
		var textFieldBackToMainMenu:TextField = new TextField();
		var shapeBackToMainMenu:Shape = new Shape();
		
		textFormatWhite.size  = (iAppWidth / 12);
		textFormatBlack.size  = (iAppWidth / 16);
			
		shapeBackToMainMenu.graphics.beginFill( 0x111111, 0 );
		shapeBackToMainMenu.graphics.drawRect( 0, 0, iAppWidth, ((textFormatWhite.size as int)*2) );
		shapeBackToMainMenu.graphics.endFill();
		
		textFieldBackToMainMenu.text   = "◄ Back to Main Menu";
		textFieldBackToMainMenu.width  = shapeBackToMainMenu.width;
		textFieldBackToMainMenu.height = shapeBackToMainMenu.height;
		
		spriteBackToMainMenu.filters   = [dropShadowFilter];
		spriteBackToMainMenu.addChild( shapeBackToMainMenu );
		spriteBackToMainMenu.addChild( textFieldBackToMainMenu );
		spriteBackToMainMenu.addEventListener( MouseEvent.CLICK, OnBackToMainMenuClick );
		spriteBackToMainMenu.mouseChildren = false;
		spriteBackToMainMenu.buttonMode = true;
		
		spriteTextFieldsHighScores.filters = [dropShadowFilter];
		spriteTextFieldsHighScores.mouseChildren = false;
		
		urlloaderXML.load(urlrequestXML);
		urlloaderXML.addEventListener(Event.COMPLETE, OnXMLFileLoaded );
		
		stateSpriteGameTetrisMainMenu.Initialize();
		stateSpriteGameTetris1PlayerLoop.Initialize();
		stateSpriteGameTetrisHighScore.Initialize();
		
	}
	
	public static function get Instance():SharedDataGameTetris
	{
		if( instance == null )
		{
			instance = new SharedDataGameTetris();
		}
			
      	return instance;
           	
	}
	
	public function OnMenuItemMouseOver( mouseEvent:MouseEvent ):void
	{
		var spriteTextFieldMenuItem:Sprite = (mouseEvent.target.valueOf() as Sprite);
		var arrayFilters:Array = spriteTextFieldMenuItem.filters;
		
		arrayFilters.push( SharedDataGameTetris.Instance.glowFilter );
		spriteTextFieldMenuItem.filters = arrayFilters;
		
	}
	
	public function OnBackToMainMenuClick( mouseEvent:MouseEvent ):void
	{
		stateMachine.ChangeState( stateSpriteGameTetrisMainMenu );
		
	}
	
	public function OnMenuItemMouseOut( mouseEvent:MouseEvent ):void
	{
		var spriteTextFieldMenuItem:Sprite = (mouseEvent.target.valueOf() as Sprite);
		var arrayFilters:Array = spriteTextFieldMenuItem.filters;
		
		arrayFilters.pop();
		spriteTextFieldMenuItem.filters = arrayFilters;
		
	}
	
	private function OnXMLFileLoaded( evnt:Event ):void
    {
    	var uiScore:uint;
    	var uiYOffset:uint;
    	var ldr:URLLoader;
    	var textField:TextField;
    	var shapeBackground:Shape;
    	    	
    	uiYOffset = 0;
    	ldr = evnt.target as URLLoader;
    	
    	shapeBackground = new Shape();
    	shapeBackground.graphics.beginFill( 0xffffff, 0.0 );
		shapeBackground.graphics.drawRect( ( 0                                                            ),
										   ( 0                                                            ),
										   ( SharedDataGameTetris.Instance.iAppWidth                      ),
										   ((SharedDataGameTetris.Instance.textFormatWhite.size as int) * 7.50 )  );
		shapeBackground.graphics.endFill( );
		
		spriteTextFieldsHighScores.addChild( shapeBackground );
		
		if (ldr != null)
		{
			bIsXMLFileLoaded = true;
			xmlHighScores = new XML(ldr.data);			
			
			for( var uiTextFieldIndex:uint = 0;
			         uiTextFieldIndex      < 5;
			         uiTextFieldIndex      ++   )
			
			{
				textField = new TextField();

				textField.autoSize = TextFieldAutoSize.CENTER;
				textField.height = (SharedDataGameTetris.Instance.textFormatWhite.size as uint) * 2;
				textField.y = uiYOffset;
				textField.text = xmlHighScores.score[uiTextFieldIndex].toString();
				
				spriteTextFieldsHighScores.addChild( textField );
				textField.x =  (spriteTextFieldsHighScores.width / 2) - (textField.width / 2)
				
				uiYOffset += ( textField.height * 1.25 );

			}

		}
		
    }
	
	
}

class StateGameTetrisInitialize implements IState
{
	public function Enter():void
	{
	}
	
	public function Exit():void
	{
	}
	
	public function Update( time:Number ):void
	{
		SharedDataGameTetris.Instance.stateMachine.ChangeState( SharedDataGameTetris.Instance.stateSpriteGameTetrisMainMenu );
		
	}
	
	public function addChildren():void
	{
	}
	
	public function removeChildren():void
	{
	}
	
}

import flash.display.Graphics;
import flash.display.Shape;
import flash.geom.Point;

class BlockWritter
{
	public  var iBlockWidth:int;
	public  var iBlockHeight:int;
	private var arrayArrayBlockLettersFont:Array;
	
	public function BlockWritter()
	{
		arrayArrayBlockLettersFont = new Array();

		arrayArrayBlockLettersFont['a'] = [  ( new Point(0,1) ),
										     ( new Point(0,2) ),
										     ( new Point(0,3) ),
										     ( new Point(0,4) ),
										     ( new Point(1,0) ),
										     ( new Point(1,2) ),
										     ( new Point(2,1) ),
										     ( new Point(2,2) ),
										     ( new Point(2,3) ),
										     ( new Point(2,4) )  ];
		
		arrayArrayBlockLettersFont['e'] = [  ( new Point(0,0) ),
										     ( new Point(0,1) ),
										     ( new Point(0,2) ),
										     ( new Point(0,3) ),
										     ( new Point(0,4) ),
										     ( new Point(1,0) ),
										     ( new Point(1,2) ),
										     ( new Point(1,4) ),
										     ( new Point(2,0) ),
										     ( new Point(2,4) )  ];

		arrayArrayBlockLettersFont['g'] = [  ( new Point(0,0) ),
										     ( new Point(0,1) ),
										     ( new Point(0,2) ),
										     ( new Point(0,3) ),
										     ( new Point(0,4) ),
										     ( new Point(1,0) ),
										     ( new Point(1,4) ),
										     ( new Point(2,0) ),
										     ( new Point(2,3) ),
										     ( new Point(2,4) )  ];										     

		arrayArrayBlockLettersFont['i'] = [  ( new Point(0,0) ),
										     ( new Point(0,4) ),
										     ( new Point(1,0) ),
										     ( new Point(1,1) ),
										     ( new Point(1,2) ),
										     ( new Point(1,3) ),
										     ( new Point(1,4) ),
										     ( new Point(2,0) ),	
										     ( new Point(2,4) )  ];
										     								   
		arrayArrayBlockLettersFont['m'] = [  ( new Point(0,0) ),
										     ( new Point(0,1) ),
										     ( new Point(0,2) ),
										     ( new Point(0,3) ),
										     ( new Point(0,4) ),
										     ( new Point(1,1) ),
										     ( new Point(2,0) ),
										     ( new Point(2,1) ),
										     ( new Point(2,2) ),										     
										     ( new Point(2,3) ),
										     ( new Point(2,4) )  ];

		arrayArrayBlockLettersFont['o'] = [  ( new Point(0,0) ),
										     ( new Point(0,1) ),
										     ( new Point(0,2) ),
										     ( new Point(0,3) ),
										     ( new Point(0,4) ),
										     ( new Point(1,0) ),
										     ( new Point(1,4) ),
										     ( new Point(2,0) ),
										     ( new Point(2,1) ),
										     ( new Point(2,2) ),										     
										     ( new Point(2,3) ),
										     ( new Point(2,4) )  ];

		arrayArrayBlockLettersFont['r'] = [  ( new Point(0,0) ),
										     ( new Point(0,1) ),
										     ( new Point(0,2) ),
										     ( new Point(0,3) ),
										     ( new Point(0,4) ),
										     ( new Point(1,0) ),
										     ( new Point(1,2) ),
										     ( new Point(2,1) ),
										     ( new Point(2,3) ),
										     ( new Point(2,4) )  ];
										     
		arrayArrayBlockLettersFont['s'] = [  ( new Point(1,0) ),
										     ( new Point(2,0) ),
										     ( new Point(0,1) ),
										     ( new Point(1,2) ),
										     ( new Point(2,3) ),
										     ( new Point(0,4) ),
										     ( new Point(1,4) )  ];
										    										    
		arrayArrayBlockLettersFont['t'] = [  ( new Point(0,0) ),
										     ( new Point(2,0) ),
										     ( new Point(1,0) ),
										     ( new Point(1,1) ),
										     ( new Point(1,2) ),
										     ( new Point(1,3) ),
										     ( new Point(1,4) )  ];
		
		arrayArrayBlockLettersFont['v'] = [  ( new Point(0,0) ),
										     ( new Point(0,1) ),
										     ( new Point(0,2) ),
										     ( new Point(0,3) ),
										     ( new Point(1,4) ),
										     ( new Point(2,0) ),
										     ( new Point(2,1) ),
										     ( new Point(2,2) ),
										     ( new Point(2,3) )  ];
	}
	
	private function DrawBlock( iX:int, iY:int, displayObjectContainer:DisplayObjectContainer ):void
	{
		var block:Block = new Block( this.iBlockWidth, this.iBlockHeight );
		displayObjectContainer.addChild( block );
		block.x = iX;
		block.y = iY;
		
	}
	
	public function WriteString( string:String, iX:int, iY:int, displayObjectContainer:DisplayObjectContainer ):void
	{
		var iLetterIndex:int;
		var arrayShapeLetterBlocks:Array = new Array();
		
		for( iLetterIndex = 0;
			 iLetterIndex < string.length;
			 iLetterIndex ++               )
		{
			WriteCharacter(  (string.charAt(iLetterIndex)                ),
							 (iX + (iLetterIndex * this.iBlockWidth * 4) ),
							 (iY                                         ),
							 (displayObjectContainer                     )  ); 
		}
	}
	
	public function WriteCharacter( char:String, iX:int, iY:int, displayObjectContainer:DisplayObjectContainer ):void
	{
		var iPointIndex:int;
		var pointCurrent:Point;
		
		if(  ( char.charAt(0) != null                              ) &&
			 ( arrayArrayBlockLettersFont[ char.charAt(0) ]!= null ) &&
			 ( char.charAt(0) != ' '                               )     )
		{
			for( iPointIndex = 0;
				 iPointIndex < (arrayArrayBlockLettersFont[ char.charAt(0) ] as Array).length;
				 iPointIndex ++                                                                )
			{
				pointCurrent = (arrayArrayBlockLettersFont[ char.charAt(0) ] as Array)[iPointIndex];
				
				DrawBlock(  ( iX + ( pointCurrent.x * this.iBlockWidth  ) ),
						    ( iY + ( pointCurrent.y * this.iBlockHeight ) ),
						    ( displayObjectContainer                      )  );
						   
			}
			
		}

	}
	
	
}

import flash.geom.Point;
class SpriteDispatchedTetrad extends Sprite
{
	public var Points:Array;
	public var Position:Point;
	public var tetradType:TetradType;
	
	public function SpriteDispatchedTetrad()
	{
		var point:Point;
		
		Position = new Point();
		Points   = new Array();
		
		point = new Point();
		Points.push( point );
		point = new Point();
		Points.push( point );
		point = new Point();
		Points.push( point );
		point = new Point();
		Points.push( point );
		
	}	

}

class SharedDataPlayPannel
{
	public var spritePlayPannel:SpritePlayPannel;

	public var arrUsedBlocks:Array;
	public var arrUnusedBlocks:Array;
	
	public var bIsGameOver:Boolean;
	public var bIsTetradDispatched:Boolean;
		
	public var iNextTetradType:int;
	
	public var nTimeEllapsed:Number;
	public var nTimeUpdate:Number;
	
	public var stateMachine:StateMachine;
	public var statePlayPannelPlay:StatePlayPannelPlay;
	public var statePlayPannelGameOver:StatePlayPannelGameOver;
	
	
	public function SharedDataPlayPannel()
	{
		arrUsedBlocks           = new Array();
		arrUnusedBlocks         = new Array();
		
		bIsGameOver             = false;
		
		stateMachine            = new StateMachine();
		statePlayPannelPlay     = new StatePlayPannelPlay();
		statePlayPannelGameOver = new StatePlayPannelGameOver();
		
		nTimeEllapsed           = 0;
		nTimeUpdate             = 20;
		
		statePlayPannelPlay.sharedDataPlayPannel = this;
		statePlayPannelGameOver.sharedDataPlayPannel = this;
		
	}
	
	public function Initialize():void
	{
		statePlayPannelGameOver.Initialize();
		iNextTetradType = Math.random() * SharedDataGameTetris.Instance.QTY_TETRADTYPES;
		CreateUnusedBlocks();
		
	}

	
	public function Reset():void
	{
		var block:Block;
		
		statePlayPannelPlay.Reset();
		statePlayPannelGameOver.Reset();
			
		bIsGameOver   = false;
		nTimeEllapsed = 0;
		stateMachine.ChangeState( statePlayPannelPlay );
		bIsTetradDispatched = false;
		
		while( arrUsedBlocks.length > 0 )
		{
			block = arrUsedBlocks.pop();
			
			block.bIsGrayscale = false;
			block.uiColorPrev  = GenerateRandomColor();
			block.uiColorNext  = GenerateRandomColor();
			block.visible      = false;
			block.x            = 0;
			block.y            = 0;
			
			arrUnusedBlocks.push( block );
			
		}
		
	}
	
	private function CreateUnusedBlocks():void
	{
		var block:Block;
		var uiQtyBlocks:uint = (  (SharedDataGameTetris.Instance.QTY_ROWS   ) *
								  (SharedDataGameTetris.Instance.QTY_COLUMNS)   );
		for( var uiBlockIndex:uint = 0;
		         uiBlockIndex      < uiQtyBlocks;
		         uiBlockIndex      ++             )
		{
			block = new Block(  (SharedDataGameTetris.Instance.iBlockWidth ),
			                    (SharedDataGameTetris.Instance.iBlockHeight)  );
			
			arrUnusedBlocks.push( block );
			
		}
		
	}
	
	
}

class SpriteGameOver extends Sprite
{

	private var spriteBlockTextGameOver:Sprite;
	private var shapeBlockTextGameOverBackground:Shape;
	

	
	public function SpriteGameOver()
	{	
		spriteBlockTextGameOver            = new Sprite();
		shapeBlockTextGameOverBackground   = new Shape(); 
		

		
	}
	
	public function Initialize():void
	{		
		spriteBlockTextGameOver = new Sprite();
		
		SharedDataGameTetris.Instance.blockWritter.iBlockWidth  = SharedDataGameTetris.Instance.iAppWidth  / 34;
		SharedDataGameTetris.Instance.blockWritter.iBlockHeight = SharedDataGameTetris.Instance.iAppHeight / 34;
		
		shapeBlockTextGameOverBackground.graphics.beginFill( 0xffffff, 0.0 ); // TODO
		shapeBlockTextGameOverBackground.graphics.drawRect(  0,
										   	   				 0,
										   	  				 SharedDataGameTetris.Instance.iAppWidth,
										  	  				(SharedDataGameTetris.Instance.blockWritter.iBlockHeight * 9) );
		shapeBlockTextGameOverBackground.graphics.endFill();
		spriteBlockTextGameOver.addChild( shapeBlockTextGameOverBackground );
		this.addChild( spriteBlockTextGameOver );
		

		
		SharedDataGameTetris.Instance.blockWritter.WriteString(  ( "game over"                                                ),
								   								 ( SharedDataGameTetris.Instance.blockWritter.iBlockWidth * 2 ),
														    	 ( SharedDataGameTetris.Instance.blockWritter.iBlockHeight* 2 ),
								   								 ( spriteBlockTextGameOver                                    )  );
								   								 
		spriteBlockTextGameOver.filters = [ SharedDataGameTetris.Instance.dropShadowFilter ];

	
	}

	
}


class StatePlayPannelGameOver implements IState
{
	public var spriteGameOver:SpriteGameOver;
	public var sharedDataPlayPannel:SharedDataPlayPannel;
	
	private var spriteTextFieldYourScore:Sprite;
	private var shapeBackground:Shape;
	private var nUpdateInterval:Number;
	private var nCurrentIntreval:Number;

	
	public function StatePlayPannelGameOver()
	{
		spriteGameOver             = new SpriteGameOver();

		spriteTextFieldYourScore   = new Sprite();
		shapeBackground            = new Shape();
		nUpdateInterval            = 100;
		nCurrentIntreval           = 0;
			
	}
	
	public function Initialize():void
	{
		InitializeSpriteGameOver();
		InitializeYourScore();
		InitializeBackground();

		
	}
	
	public function Enter():void
	{
		spriteGameOver.visible                                           = false;
		SharedDataGameTetris.Instance.spriteTextFieldsHighScores.visible = false;		
		SharedDataGameTetris.Instance.spriteBackToMainMenu.visible       = false;
		
		addChildren();
		
		SharedDataGameTetris.Instance.spriteBackToMainMenu.addEventListener(MouseEvent.CLICK, OnBackToMainMenuClick );

		for( var uiUsedBlocksIndex:uint = 0;
		         uiUsedBlocksIndex      < sharedDataPlayPannel.arrUsedBlocks.length;
		         uiUsedBlocksIndex      ++                                           )
		{
			(sharedDataPlayPannel.arrUsedBlocks[ uiUsedBlocksIndex ] as Block).bIsGrayscale = true;		
		}
		
	}
	
	public function Exit():void
	{
		removeChildren();
		
	}
		
	public function Update( nTimeEllapsed:Number ):void
	{
		var textField:TextField;
		
		nCurrentIntreval += nTimeEllapsed;
		
		if( nCurrentIntreval >=	nUpdateInterval )
		{
			nCurrentIntreval = 0;
			
			if(shapeBackground.alpha < .60)
			{			
				shapeBackground.alpha += 0.03;
				
				if(shapeBackground.alpha >= .60)
				{
					spriteGameOver.visible = true;
					
					for( var uiTextFieldIndex:uint = 1;
				 			 uiTextFieldIndex < SharedDataGameTetris.Instance.spriteTextFieldsHighScores.numChildren;
				 			 uiTextFieldIndex ++                                                                      )
					{
						textField = SharedDataGameTetris.Instance.spriteTextFieldsHighScores.getChildAt( uiTextFieldIndex ) as TextField;
						textField.setTextFormat( SharedDataGameTetris.Instance.textFormatBlack );
						
					}
				
					SharedDataGameTetris.Instance.spriteTextFieldsHighScores.y = ( spriteTextFieldYourScore.y + spriteTextFieldYourScore.height );					
					SharedDataGameTetris.Instance.spriteTextFieldsHighScores.visible = true;
					
					textField = (SharedDataGameTetris.Instance.spriteBackToMainMenu.getChildAt(1) as TextField);
					textField.setTextFormat( SharedDataGameTetris.Instance.textFormatBlack );
					SharedDataGameTetris.Instance.spriteBackToMainMenu.y =
						( SharedDataGameTetris.Instance.iAppHeight - ((SharedDataGameTetris.Instance.textFormatBlack.size as int) * 1.5) );
					SharedDataGameTetris.Instance.spriteBackToMainMenu.visible = true;						
					
				}
				
			}
			else
			{


			}
			
		}
		
	}

	public function Reset():void
	{
		nCurrentIntreval      =   0;
		shapeBackground.alpha = 0.0;
		
	}

	public function addChildren():void
	{
		SharedDataGameTetris.Instance.spriteGameTetris.addChild( shapeBackground );
		SharedDataGameTetris.Instance.spriteGameTetris.addChild( spriteGameOver );
		SharedDataGameTetris.Instance.spriteGameTetris.addChild( spriteTextFieldYourScore );
		SharedDataGameTetris.Instance.spriteGameTetris.addChild( SharedDataGameTetris.Instance.spriteTextFieldsHighScores );
		SharedDataGameTetris.Instance.spriteGameTetris.addChild( SharedDataGameTetris.Instance.spriteBackToMainMenu );
		
	}
	
	public function removeChildren():void
	{
		
		SharedDataGameTetris.Instance.spriteGameTetris.removeChild( shapeBackground );
		SharedDataGameTetris.Instance.spriteGameTetris.removeChild( spriteGameOver );
		SharedDataGameTetris.Instance.spriteGameTetris.removeChild( spriteTextFieldYourScore );
		SharedDataGameTetris.Instance.spriteGameTetris.removeChild( SharedDataGameTetris.Instance.spriteTextFieldsHighScores );
		SharedDataGameTetris.Instance.spriteGameTetris.removeChild( SharedDataGameTetris.Instance.spriteBackToMainMenu );

	}
	
	private function InitializeBackground():void
	{
		shapeBackground.alpha = 0;
		shapeBackground.graphics.beginFill( 0xffffff, 1 );
		shapeBackground.graphics.drawRect( 0,
										   0,
										   SharedDataGameTetris.Instance.iAppWidth,
										   SharedDataGameTetris.Instance.iAppHeight );
		shapeBackground.graphics.endFill();
		
		sharedDataPlayPannel.spritePlayPannel.addChild( shapeBackground );	
		
	}
	
	private function InitializeSpriteGameOver():void
	{
		spriteGameOver.Initialize();
		
	}

	private function InitializeYourScore():void
	{
		var textFieldYourScore:TextField   = new TextField();
		var shapeYourScoreBackground:Shape = new Shape();

		shapeYourScoreBackground.graphics.beginFill( 0x0000ff, 1 );
		shapeYourScoreBackground.graphics.drawRect(  0,
										             0,
									                 SharedDataGameTetris.Instance.iAppWidth,
										           ((SharedDataGameTetris.Instance.textFormatBlack.size as int) * 2)  );
		shapeYourScoreBackground.graphics.endFill();
		
		textFieldYourScore.setTextFormat( SharedDataGameTetris.Instance.textFormatBlack );
		
		spriteTextFieldYourScore.addChild( shapeYourScoreBackground );		
		spriteTextFieldYourScore.addChild( textFieldYourScore );
		textFieldYourScore.width  = shapeYourScoreBackground.width;
		textFieldYourScore.height = shapeYourScoreBackground.height;
		
		spriteTextFieldYourScore.y = ( spriteGameOver.y + spriteGameOver.height ); //ojo
		
		textFieldYourScore.text = "poopie";  //TODO				
		
	}
	
	private function OnBackToMainMenuClick( mouseEvent:MouseEvent ):void
	{
		SharedDataGameTetris.Instance.spriteBackToMainMenu.removeEventListener( MouseEvent.CLICK, this.OnBackToMainMenuClick );
		sharedDataPlayPannel.stateMachine.ChangeState( sharedDataPlayPannel.statePlayPannelPlay );
	}
	
	
}

class StatePlayPannelPlay implements IState
{
	public var sharedDataPlayPannel:SharedDataPlayPannel;

	private var bVerticalCollision:Boolean;
	private var iDeltaRotation:int;
	private var pDeltaMovement:Point;
	private var spriteDispatchedTetrad:SpriteDispatchedTetrad;
	

	public function StatePlayPannelPlay()
	{
		spriteDispatchedTetrad = new SpriteDispatchedTetrad();
		pDeltaMovement         = new Point();

	}
	
	public function Enter():void
	{
		addChildren();
		
	}
	
	public function Exit():void
	{
		removeChildren();
		
	}
	
	public function Update( time:Number ):void
	{
		if( sharedDataPlayPannel.bIsTetradDispatched == true )
		{
			bVerticalCollision = false;

			pDeltaMovement.x = 0;
			pDeltaMovement.y = 0;
			iDeltaRotation   = 0;
			
			//PoolInput();
			/*
			Pool Input
			 */
			 
			 /*ProcessInput*/		
			
			sharedDataPlayPannel.nTimeEllapsed += time;		
			
			if( sharedDataPlayPannel.nTimeEllapsed >= sharedDataPlayPannel.nTimeUpdate )
			{
				sharedDataPlayPannel.nTimeEllapsed = 0;
				UpdateGravity();
				
			}
			
			CheckCollisions();
			UpdateDispatchedTetrad();
			
			if( bVerticalCollision == true )
			{
				if( IsGameOver() )
				{
					MoveTetradBlocksToUsedBlockArray();
					sharedDataPlayPannel.bIsGameOver = true;
					sharedDataPlayPannel.stateMachine.ChangeState( sharedDataPlayPannel.statePlayPannelGameOver );
	
				}
				else
				{
					MoveTetradBlocksToUsedBlockArray();

					sharedDataPlayPannel.bIsTetradDispatched = false;
					sharedDataPlayPannel.iNextTetradType = Math.random() * SharedDataGameTetris.Instance.QTY_TETRADTYPES;

				}
	
			}
				
		}
		else
		{
			DispatchTetrad( sharedDataPlayPannel.iNextTetradType );
			
		}

	}
	
	public function addChildren():void
	{
		sharedDataPlayPannel.spritePlayPannel.addChild( spriteDispatchedTetrad );

	}
	
	public function removeChildren():void
	{
		sharedDataPlayPannel.spritePlayPannel.removeChild( spriteDispatchedTetrad );

	}
	
	public function Reset():void
	{
		pDeltaMovement.x   = 0;
		pDeltaMovement.y   = 0;
		bVerticalCollision = false;

	}
	
	private function CheckBottomCollision():Boolean
	{
		var iBlockYPosition:int;
		
		for( var uiBlockIndex:uint = 0;
		         uiBlockIndex      < 4;
		         uiBlockIndex      ++   )
		{
			iBlockYPosition = (  ( spriteDispatchedTetrad.Points[uiBlockIndex].y ) + 
			                     ( spriteDispatchedTetrad.Position.y             ) +
			                     ( pDeltaMovement.y                              )    );
			
			if( iBlockYPosition >= SharedDataGameTetris.Instance.QTY_ROWS )
			{
				pDeltaMovement.x = 0;
				pDeltaMovement.y = 0;

				return true;

			}

		}		

		return false;

	}
	
	private function CheckCollisions():void
	{
		CheckSidesCollisions();
		if( CheckUsedBlockCollisions() ||
		    CheckBottomCollision()        )
		{
			bVerticalCollision = true;
			
		}		
		
	}
	
	private function CheckSidesCollisions():Boolean
	{
		var iBlockXPosition:int;
		
		for( var uiBlockIndex:uint = 0;
		         uiBlockIndex      < 4;
		         uiBlockIndex      ++   )
		{
			iBlockXPosition = (  ( spriteDispatchedTetrad.Points[uiBlockIndex].x ) +
								 ( spriteDispatchedTetrad.Position.x             ) +
								 ( pDeltaMovement.x                              )    );
			
			if( (iBlockXPosition < 0                                          )||
			    (iBlockXPosition >= SharedDataGameTetris.Instance.QTY_COLUMNS )   )
			{
				pDeltaMovement.x = 0;
								
				return true;
				
			}
						
		}		
		
		return false;
		
	}
	
	private function CheckUsedBlockCollisions():Boolean
	{
		var iBlockXPosition:int;
		var iBlockYPosition:int;
		var blockUsed:Block;
		
		for( var uiUsedBlockIndex:uint = 0;
		         uiUsedBlockIndex      < sharedDataPlayPannel.arrUsedBlocks.length;
		         uiUsedBlockIndex      ++                                           )
		{
			blockUsed = sharedDataPlayPannel.arrUsedBlocks[ uiUsedBlockIndex ] as Block;
			
			for( var uiBlockIndex:uint = 0;
		             uiBlockIndex      < 4;
		             uiBlockIndex      ++   )
			{
				iBlockXPosition = (  ( spriteDispatchedTetrad.Points[uiBlockIndex].x )+ 
				                     ( spriteDispatchedTetrad.Position.x             )+ 
				                     ( pDeltaMovement.x                              )   );
				                     
				iBlockYPosition = (  ( spriteDispatchedTetrad.Points[uiBlockIndex].y )+
				                     ( spriteDispatchedTetrad.Position.y             )+
				                     ( pDeltaMovement.y                              )   );			

				if( ( blockUsed.Column == iBlockXPosition ) &&
					( blockUsed.Row    == iBlockYPosition )    )
				{
					pDeltaMovement.x = 0;
					pDeltaMovement.y = 0;					
						
					return true;
						
				}
				
			}
				
		}
		
		return false;
		
	}
	
	private function DispatchTetrad( uiTetradType:uint ):void
	{
		var block0:Block;
		var block1:Block;
		var block2:Block;
		var block3:Block;
				
		var blockN:Block;
		
		sharedDataPlayPannel.bIsTetradDispatched = true;
		
		spriteDispatchedTetrad.Position.x =  5;
		spriteDispatchedTetrad.Position.y = -1;
		
		PositionTetradSprite();
		
		block0 = sharedDataPlayPannel.arrUnusedBlocks.pop();
		block1 = sharedDataPlayPannel.arrUnusedBlocks.pop();
		block2 = sharedDataPlayPannel.arrUnusedBlocks.pop();
		block3 = sharedDataPlayPannel.arrUnusedBlocks.pop();

		block0.visible = false;
		block1.visible = false;
		block2.visible = false;
		block3.visible = false;

		spriteDispatchedTetrad.tetradType = SharedDataGameTetris.Instance.arrayTetradTypes[ uiTetradType ];
		
		spriteDispatchedTetrad.addChild( block0 );
		spriteDispatchedTetrad.addChild( block1 );
		spriteDispatchedTetrad.addChild( block2 );				
		spriteDispatchedTetrad.addChild( block3 );		

		for( var uiBlockIndex:uint = 1;
				 uiBlockIndex      < 4;
				 uiBlockIndex      ++   )
		{
			spriteDispatchedTetrad.Points[ uiBlockIndex ].x = (spriteDispatchedTetrad.tetradType.arrayPointBlocks[ uiBlockIndex - 1 ] as Point).x;
			spriteDispatchedTetrad.Points[ uiBlockIndex ].y = (spriteDispatchedTetrad.tetradType.arrayPointBlocks[ uiBlockIndex - 1 ] as Point).y;
			
			blockN = ( spriteDispatchedTetrad.getChildAt( uiBlockIndex ) as Block );
			
			blockN.x = ( block0.x                                            ) + 
			           (  ( spriteDispatchedTetrad.Points[ uiBlockIndex ].x  ) *
			              ( SharedDataGameTetris.Instance.iBlockWidth        )   );
			                                       
			blockN.y = ( block0.y                                            ) + 
			           (  ( spriteDispatchedTetrad.Points[ uiBlockIndex ].y  ) *
			              ( SharedDataGameTetris.Instance.iBlockHeight       )   );

		}
		
						
	}
	
	private function IsGameOver():Boolean
	{	
		var block:Block;
			
		for( var uiBlockIndex:uint = 0;
				 uiBlockIndex      < 4;
				 uiBlockIndex        ++ )
		{
			if(  ( spriteDispatchedTetrad.Points[ uiBlockIndex ].y ) +
			     ( spriteDispatchedTetrad.Position.y               )   < 0)
			{
				return true;

			}

		}

		return false;
		
	}
	
	private function MoveTetradBlocksToUsedBlockArray():void
	{	
		var block:Block;
		
		for( var uiBlockIndex:uint = 0;
				 uiBlockIndex      < 4;
				 uiBlockIndex       ++  )
		{
			block = ( spriteDispatchedTetrad.removeChildAt( 0 ) as Block );
			
			sharedDataPlayPannel.arrUsedBlocks.push( block );			
			sharedDataPlayPannel.spritePlayPannel.addChild( block );
			
			block.Row    = (  ( spriteDispatchedTetrad.Position.y                    )+
					          ( spriteDispatchedTetrad.Points[uiBlockIndex] as Point ).y  );
			
			block.Column = (  ( spriteDispatchedTetrad.Position.x                    )+
					     	  ( spriteDispatchedTetrad.Points[uiBlockIndex] as Point ).x  );
					     	  
			block.x = (  ( block.Column                               )*
					     ( SharedDataGameTetris.Instance.iBlockWidth  )  );
					  
			block.y = (  ( block.Row                                  )*
					     ( SharedDataGameTetris.Instance.iBlockHeight )  );											  
					  
		}

	}
	
	private function PositionTetradSprite():void
	{
		spriteDispatchedTetrad.x = (  ( spriteDispatchedTetrad.Position.x          ) *
		                              ( SharedDataGameTetris.Instance.iBlockWidth  )    );
		                              
		spriteDispatchedTetrad.y = (  ( spriteDispatchedTetrad.Position.y          ) *
		                              ( SharedDataGameTetris.Instance.iBlockHeight )    );
		
	}		
	
	private function UpdateGravity():void
	{
		pDeltaMovement.x  = 0;
		pDeltaMovement.y += 1;

	}
	
	private function UpdateDispatchedTetrad():void
	{
		var block:Block;
		var iYBlockPosition:int;
		
		spriteDispatchedTetrad.Position.x += pDeltaMovement.x;
		spriteDispatchedTetrad.Position.y += pDeltaMovement.y;
		
		spriteDispatchedTetrad.x += pDeltaMovement.x * SharedDataGameTetris.Instance.iBlockWidth;
		spriteDispatchedTetrad.y += pDeltaMovement.y * SharedDataGameTetris.Instance.iBlockHeight;
		
		for( var uiBlockIndex:uint = 0;
			     uiBlockIndex      < 4;
			     uiBlockIndex      ++  )
		{
			block = ( spriteDispatchedTetrad.getChildAt( uiBlockIndex ) as Block );
			
			iYBlockPosition = spriteDispatchedTetrad.y + ( spriteDispatchedTetrad.Points[uiBlockIndex].y * SharedDataGameTetris.Instance.iBlockHeight );
			
			if( iYBlockPosition < 0 )
			{
				block.visible = false;
				
			}
			else
			{
				block.visible = true;
				
			}
			
		}		
		
	}

	
}

import flash.display.Sprite;
class SpritePlayPannel extends Sprite
{
	public  var sharedDataPlayPannel:SharedDataPlayPannel;
	
	private var shapePannelBackground:Shape;

	
	public function SpritePlayPannel()
	{
		sharedDataPlayPannel  = new SharedDataPlayPannel();
		sharedDataPlayPannel.spritePlayPannel = this; 
		shapePannelBackground = new Shape();
						
	}
	
	public function Initialize():void
	{	
		InsertBackground();
		
		sharedDataPlayPannel.Initialize();
		sharedDataPlayPannel.stateMachine.ChangeState( sharedDataPlayPannel.statePlayPannelPlay );
			
	}
	
	public function Update( nTime:Number ):void
	{
		sharedDataPlayPannel.stateMachine.Update( nTime );	
		
	}
	
	public function Reset():void
	{
		sharedDataPlayPannel.Reset();
		
	}
	
	private function InsertBackground():void
	{
		var iWidth:int  = (SharedDataGameTetris.Instance.iAppWidth / 2);	
		var iHeight:int = (SharedDataGameTetris.Instance.iAppHeight   );
		
		shapePannelBackground.graphics.lineStyle( 1, 0xffffff, 1 );
		shapePannelBackground.graphics.moveTo(0, 0);
		shapePannelBackground.graphics.lineTo(0, iHeight);
		shapePannelBackground.graphics.lineTo(iWidth, iHeight);	
		shapePannelBackground.graphics.lineTo(iWidth, 0);
		shapePannelBackground.graphics.lineTo(0, 0);
		
		shapePannelBackground.graphics.beginFill( 0xffffff, 0.5 );
		shapePannelBackground.graphics.drawRect( 0, 0, iWidth, iHeight );
		shapePannelBackground.graphics.endFill( );
		
		this.addChild( shapePannelBackground );
		
	}


}

import flash.display.Sprite;
class SpriteGameInfoPannel extends Sprite
{
	private var arrayBlocks:Array;
	private var arraySpritePercentageBars:Array;
	private var arrayUintDispatchedTetradTypeQty:Array; 
	private var shapePannelBackground:Shape;
	private var spriteNextTetrad:SpriteNextTetrad;	
	private var uiDispatchedTetradsQty:uint;
		
	public function SpriteGameInfoPannel()
	{
		arrayBlocks                      = new Array();
		arraySpritePercentageBars        = new Array();
		arrayUintDispatchedTetradTypeQty = new Array();
		
		shapePannelBackground            = new Shape();
		
	}
	
	public function GrayscaleBlocks():void
	{
		for( var uiBlockIndex:uint = 0;
			     uiBlockIndex      < arrayBlocks.length;
			     uiBlockIndex      ++                    )
		{
			( arrayBlocks[ uiBlockIndex ] as Block ).bIsGrayscale = true;
		}
		
		spriteNextTetrad.GrayscaleBlocks();
		
	}
	
	public function Initialize():void
	{	
		for( var uiTetradTypeIndex:uint = 0;
				 uiTetradTypeIndex      < SharedDataGameTetris.Instance.QTY_TETRADTYPES;
				 uiTetradTypeIndex        ++                                             )
		{
			arrayUintDispatchedTetradTypeQty.push( 0 );
		}
		
		InsertBackground();	
		InsertTetradTypes();
		InsertPercentageBars();
		InsertNextTetrad();
		
	}
	
	public function Reset():void
	{
		ColorTetradTypes();
		ColorNextTetrad();
		ResetPercentageBars();
		
	}
	
	public function set NextTetrad( uiTetradType:uint ):void
	{
		spriteNextTetrad.NextTetradType = uiTetradType;
		
	}
	
	public function get NextTetrad():uint
	{
		return spriteNextTetrad.NextTetradType;
		
	}
	
	public function get NextTetradType():uint
	{
		return spriteNextTetrad.NextTetradType;
		
	}
	
	public function set NextTetradType( uinexttetradtype:uint ):void
	{
		spriteNextTetrad.NextTetradType = uinexttetradtype;
		
	}
	
	public function UpdatePercentageBars():void
	{
		var uiTetradTypeindex:uint = spriteNextTetrad.NextTetradType;
		var uiDispatchedTetradTypeQty:uint =
			( arrayUintDispatchedTetradTypeQty[ uiTetradTypeindex ] as uint );

		uiDispatchedTetradsQty++;		
		arrayUintDispatchedTetradTypeQty[ spriteNextTetrad.NextTetradType ] += 1;
		
		for( var uiPercentageBarIndex:uint = 0;
				 uiPercentageBarIndex      < SharedDataGameTetris.Instance.QTY_TETRADTYPES;
				 uiPercentageBarIndex        ++                                             )
		{
			( arraySpritePercentageBars[ uiPercentageBarIndex ] as SpritePercentageBar ).Percentage = 
			( arrayUintDispatchedTetradTypeQty[ uiPercentageBarIndex ] as uint ) / uiDispatchedTetradsQty;
			
		}
		
	}
	
	
	private function ColorNextTetrad():void
	{
		spriteNextTetrad.ColorBlocks();
		spriteNextTetrad.SetRandomColors();
		
	}	
		
	private function ColorTetradTypes():void
	{
		var block:Block;
		
		for( var uiBlockIndex:uint = 0;
			     uiBlockIndex      < arrayBlocks.length;
			     uiBlockIndex      ++                    )
		{
			block = ( arrayBlocks[ uiBlockIndex ] as Block );
			
			block.bIsGrayscale = false;
			block.uiColorPrev  = GenerateRandomColor();
			block.uiColorNext  = GenerateRandomColor();

		}		
		
	}
	
	private function InsertBackground():void
	{	
		var iWidth:int  = (SharedDataGameTetris.Instance.iAppWidth / 2);	
		var iHeight:int = (SharedDataGameTetris.Instance.iAppHeight   );
		
		shapePannelBackground.graphics.lineStyle( 1, 0xffffff, 1 );
		shapePannelBackground.graphics.moveTo(0, 0);
		shapePannelBackground.graphics.lineTo(0, iHeight);
		shapePannelBackground.graphics.lineTo(iWidth, iHeight);	
		shapePannelBackground.graphics.lineTo(iWidth, 0);
		shapePannelBackground.graphics.lineTo(0, 0);
		
		shapePannelBackground.graphics.beginFill( 0xffffff, 0.5 );
		shapePannelBackground.graphics.drawRect( 0, 0, iWidth, iHeight );
		shapePannelBackground.graphics.endFill( );
		
		this.addChild( shapePannelBackground );
		
	}
	
	private function InsertNextTetrad():void
	{
		spriteNextTetrad = new SpriteNextTetrad(  ( (SharedDataGameTetris.Instance.iAppWidth  / 50) * 2 ),
												  ( (SharedDataGameTetris.Instance.iAppHeight / 50) * 2 )  ); 
		this.addChild( spriteNextTetrad );
		spriteNextTetrad.x = (SharedDataGameTetris.Instance.iAppWidth  / 50) * 8;
		spriteNextTetrad.y = (SharedDataGameTetris.Instance.iAppHeight / 50) * 2;
		spriteNextTetrad.NextTetradType = Math.random()*SharedDataGameTetris.Instance.QTY_TETRADTYPES;			

	}
	
	private function InsertTetradTypes():void
	{
		var block:Block;
		
		var tetradType_:TetradType;
			
		var iXPosition:uint
		var iYPosition:uint

		var iVerticalOffset:int;		
		var iHorizontalOffset:int;
		
		var iVerticalUnitLength:uint;
		var iHorizontalUnitLength:uint;

						
		iVerticalUnitLength   = SharedDataGameTetris.Instance.iAppHeight / 50;
		iHorizontalUnitLength = SharedDataGameTetris.Instance.iAppWidth  / 50;

		iXPosition = iHorizontalUnitLength *  4;
		iYPosition = iVerticalUnitLength   * 10;

		
		for( var uiTetradTypeIndex_:uint = 0;
			 	 uiTetradTypeIndex_ < SharedDataGameTetris.Instance.QTY_TETRADTYPES;
			 	 uiTetradTypeIndex_ ++                                               )
		{						
			tetradType_ = SharedDataGameTetris.Instance.arrayTetradTypes[uiTetradTypeIndex_];
			
			for( var uiBlockIndex:uint = 0;
					 uiBlockIndex      < 4;
					 uiBlockIndex ++        )
			{
				if( uiBlockIndex == 0 )
				{
					iHorizontalOffset = 0;
					iVerticalOffset   = 0;					
				}
				else
				{
					iHorizontalOffset = tetradType_.arrayPointBlocks[uiBlockIndex - 1].x;
					iVerticalOffset   = tetradType_.arrayPointBlocks[uiBlockIndex - 1].y;
				}
				
				block = new Block(  ( iHorizontalUnitLength * 2 ), 
									( iVerticalUnitLength   * 2 )  );
										
				block.x = iXPosition + ( iHorizontalOffset * ( iHorizontalUnitLength * 2 )  );
				block.y = iYPosition + ( iVerticalOffset   * ( iVerticalUnitLength   * 2 )  );				
				
				arrayBlocks.push( block );
				
				this.addChild( block );
				
			}							
			
			iYPosition += iVerticalUnitLength * 6;
			
		}
	
	}
	
	private function ResetPercentageBars():void
	{
		uiDispatchedTetradsQty = 0;
		
		for( var uiPercentageBarIndex:uint = 0;
				 uiPercentageBarIndex      < SharedDataGameTetris.Instance.QTY_TETRADTYPES;
				 uiPercentageBarIndex        ++                                             )
		{
			( arraySpritePercentageBars[ uiPercentageBarIndex ] as SpritePercentageBar ).Percentage = 0.0;
			arrayUintDispatchedTetradTypeQty[ uiPercentageBarIndex ] = 0;
			
		}

	}
	
	private function InsertPercentageBars():void
	{
		var uiVerticalUnitLength:uint;
		var uiHorizontalUnitLength:uint;
		var uiPercentageBarsVerticalOffset:uint;
		var uiPercentageBarsHorizontalOffset:uint;
		var spritePercentageBar:SpritePercentageBar;
				
		uiVerticalUnitLength   = SharedDataGameTetris.Instance.iAppHeight / 50;
		uiHorizontalUnitLength = SharedDataGameTetris.Instance.iAppWidth  / 50;
		
		uiPercentageBarsVerticalOffset   = 10 * uiVerticalUnitLength;
		uiPercentageBarsHorizontalOffset = 12 * uiHorizontalUnitLength;
		
		for( var uiTetradTypeIndex:uint = 0;
			     uiTetradTypeIndex < SharedDataGameTetris.Instance.QTY_TETRADTYPES;
			     uiTetradTypeIndex ++                                               )
		{
			spritePercentageBar = new SpritePercentageBar(  ( uiHorizontalUnitLength * 11 ),
														    ( uiVerticalUnitLength   *  2 ),
														    ( 1.00                        )  );
														   
			spritePercentageBar.x = uiPercentageBarsHorizontalOffset;
			spritePercentageBar.y = uiPercentageBarsVerticalOffset;			
			spritePercentageBar.filters = [SharedDataGameTetris.Instance.dropShadowFilter];
			
			this.addChild( spritePercentageBar );			
						
			uiPercentageBarsVerticalOffset += ( uiVerticalUnitLength * 6 );
			
			arraySpritePercentageBars.push( spritePercentageBar );
			
		}
		
	}
	
		
}

import flash.display.Sprite;
class SpriteNextTetrad extends Sprite
{
	private var arrayBlocks:Array;
	private var shapeNextTetradBackground:Shape;
	private var uiTetradType:uint;
	
	public function SpriteNextTetrad( uiBlockWidth:uint = 10,
									  uiBlockHeight:uint = 10 )
	{
		var block:Block;

		uiTetradType = 0;
		arrayBlocks = new Array();
		
		for( var iBlockIndex:int = 0;
				 iBlockIndex     < 4;
				 iBlockIndex ++       )
		{
			block = new Block( uiBlockWidth, uiBlockHeight );
			arrayBlocks.push( block );
			this.addChild( block );
			
		}
		
		shapeNextTetradBackground = new Shape();
		
		shapeNextTetradBackground.graphics.beginFill( 0xffffff, 0.0 );
		shapeNextTetradBackground.graphics.drawRect(0, 0, (uiBlockWidth * 4), (uiBlockHeight * 2));
		shapeNextTetradBackground.graphics.endFill( );

		this.addChild( shapeNextTetradBackground );

	}
	
	public function set NextTetradType( uitetradtype:uint ):void
	{
		var tetradType:TetradType;
		
		var iBlockWidth       :int;
		var iBlockHeight      :int;
		var iHorizontalOffset :int;
		var iVerticalOffset   :int;
		var iXPosition        :int;
		var iYPosition        :int;
				
		if( uitetradtype < SharedDataGameTetris.Instance.arrayTetradTypes.length )
		{
			uiTetradType = uitetradtype;
			
			tetradType = SharedDataGameTetris.Instance.arrayTetradTypes[ uitetradtype ];
			
			iBlockWidth  = (arrayBlocks[0] as Block).width;
			iBlockHeight = (arrayBlocks[0] as Block).height;
			
			iXPosition   = (  (shapeNextTetradBackground.width / 2) -
			                  (iBlockWidth                     / 2)    );
			                   
			iYPosition   = (  (shapeNextTetradBackground.height / 2) -
			                  (iBlockHeight                     / 2)    );

			this.visible = false;
			
			for( var uiBlockIndex:uint = 0;
					 uiBlockIndex      < 4;
					 uiBlockIndex      ++   )
			{
				if( uiBlockIndex == 0 )
				{
					iHorizontalOffset = 0;
					iVerticalOffset   = 0;
				}
				else
				{
					iHorizontalOffset = iBlockWidth  * (tetradType.arrayPointBlocks[ uiBlockIndex - 1 ] as Point).x;
					iVerticalOffset   = iBlockHeight * (tetradType.arrayPointBlocks[ uiBlockIndex - 1 ] as Point).y;
				}
				
				
				(arrayBlocks[uiBlockIndex] as Block).x = iXPosition + iHorizontalOffset;
				(arrayBlocks[uiBlockIndex] as Block).y = iYPosition + iVerticalOffset;
				(arrayBlocks[uiBlockIndex] as Block).uiColorPrev = GenerateRandomColor();
				(arrayBlocks[uiBlockIndex] as Block).uiColorNext = GenerateRandomColor();
				
			}
			
			this.visible = true;
		
		}
	
	}
	
	public function get NextTetradType():uint
	{
		return uiTetradType;
			
	}	
	
	public function SetRandomColors():void
	{
		var block:Block;
		
		for( var uiBlockIndex:uint = 0;
				 uiBlockIndex      < 4;
				 uiBlockIndex        ++ )
		{
			block = ( arrayBlocks[uiBlockIndex] as Block );
			
			block.uiColorPrev = GenerateRandomColor();
			block.uiColorNext = GenerateRandomColor();

		}
		
	}
	
	public function ColorBlocks():void
	{
		( arrayBlocks[ 0 ] as Block ).bIsGrayscale = false;
		( arrayBlocks[ 1 ] as Block ).bIsGrayscale = false;
		( arrayBlocks[ 2 ] as Block ).bIsGrayscale = false;
		( arrayBlocks[ 3 ] as Block ).bIsGrayscale = false;
		
	}
	
	public function GrayscaleBlocks():void
	{
		( arrayBlocks[ 0 ] as Block ).bIsGrayscale = true;
		( arrayBlocks[ 1 ] as Block ).bIsGrayscale = true;
		( arrayBlocks[ 2 ] as Block ).bIsGrayscale = true;
		( arrayBlocks[ 3 ] as Block ).bIsGrayscale = true;
		
	}


}

import flash.display.Sprite;
class SpritePercentageBar extends Sprite
{
	private var shapeBackground:Shape;
	private var shapeBar:Shape;
	private var nPercentage:Number;
	private var uiWidth:uint;
	private var uiHeight:uint;
	
	public function SpritePercentageBar( uiBarWidth:uint = 200,
										 uiBarHeight:uint = 10,
										 nInitialPercentageValue:Number = 1.00 )
	{
		uiWidth     = uiBarWidth;
		uiHeight    = uiBarHeight;
	
		shapeBackground = new Shape();
		shapeBackground.graphics.beginFill( 0xffffff, 0.0 );
		shapeBackground.graphics.drawRect( 0, 0, uiBarWidth, uiBarHeight);
		shapeBackground.graphics.endFill( );
		
		shapeBar = new Shape();
		shapeBar.graphics.beginFill( 0xffffff, 0.75 );
		shapeBar.graphics.drawRect( 0, 0, 1, uiBarHeight);
		shapeBar.graphics.endFill( );
		
		this.Percentage = nInitialPercentageValue;		
		this.addChild( shapeBackground );
		this.addChild( shapeBar );
				
	}
	
	public function get Percentage():Number
	{
		return nPercentage;
		
	}
	
	public function set Percentage( nNewPercentageValue:Number ):void
	{
		nPercentage = nNewPercentageValue;
		
		shapeBar.width = nPercentage * uiWidth;
		
	}
	
	
}

import flash.display.Sprite;
class SpriteTitle extends Sprite
{
	
	public function SpriteTitle()
	{

										   		
	}
	
	public function Initialize():void
	{
		SharedDataGameTetris.Instance.blockWritter.iBlockWidth  = SharedDataGameTetris.Instance.iAppWidth  / 25;
		SharedDataGameTetris.Instance.blockWritter.iBlockHeight = SharedDataGameTetris.Instance.iAppHeight / 25;
		
		SharedDataGameTetris.Instance.blockWritter.WriteString(  ( "tetris"                                                ),
								   								 ( SharedDataGameTetris.Instance.blockWritter.iBlockWidth  ),
														    	 ( SharedDataGameTetris.Instance.blockWritter.iBlockHeight ),
								   								 ( this                                                    )  );

	}
	
	
}

import flash.display.Shape;
import flash.display.Sprite;
import flash.text.*; 
class StateSpriteGameTetrisMainMenu extends Sprite implements IState
{
	private var spriteTitle:SpriteTitle;
	private var spriteTextField1PlayerGame:Sprite;
	private var spriteTextField2PlayerGame:Sprite;
	private var spriteTextFieldOptions:Sprite
	private var spriteTextFieldHighScores:Sprite;
	
	public function StateSpriteGameTetrisMainMenu()
	{
		
		
	}
	
	public function Initialize():void
	{
		var uiYOffset:uint = 0;
		var textfield1PlayerGame:TextField;
		var textfield2PlayersGame:TextField;
		var textFieldOptions:TextField;;
		var textFieldHighScores:TextField;	
		
		spriteTitle = new SpriteTitle();
		spriteTitle.Initialize();
		
		
		uiYOffset = spriteTitle.height * 2;
		
		spriteTextField1PlayerGame = new Sprite();
		textfield1PlayerGame = new TextField();		
		textfield1PlayerGame.text = "Single Player Game";
		textfield1PlayerGame.autoSize = TextFieldAutoSize.CENTER;
		textfield1PlayerGame.setTextFormat( SharedDataGameTetris.Instance.textFormatWhite );
		
		spriteTextField1PlayerGame.addChild( textfield1PlayerGame );
		textfield1PlayerGame.height = (SharedDataGameTetris.Instance.textFormatWhite.size as uint) * 2;
		spriteTextField1PlayerGame.x = (  (SharedDataGameTetris.Instance.iAppWidth / 2) -
										  (textfield1PlayerGame.width              / 2)    );
		spriteTextField1PlayerGame.y = uiYOffset;
		spriteTextField1PlayerGame.mouseChildren = false;
		spriteTextField1PlayerGame.buttonMode = true;
		
		uiYOffset += textfield1PlayerGame.height;

		spriteTextField2PlayerGame = new Sprite();		
		textfield2PlayersGame = new TextField();	
		textfield2PlayersGame.text = "VS Mode Game";
		textfield2PlayersGame.autoSize = TextFieldAutoSize.CENTER;
		textfield2PlayersGame.setTextFormat( SharedDataGameTetris.Instance.textFormatWhite );
		
		spriteTextField2PlayerGame.addChild( textfield2PlayersGame );
		textfield2PlayersGame.height = (SharedDataGameTetris.Instance.textFormatWhite.size as uint) * 2;
		spriteTextField2PlayerGame.x = (  (SharedDataGameTetris.Instance.iAppWidth / 2) -
										  (textfield2PlayersGame.width             / 2)    );
		spriteTextField2PlayerGame.y = uiYOffset;
		spriteTextField2PlayerGame.mouseChildren = false;
		spriteTextField2PlayerGame.buttonMode = true;
		
		uiYOffset += textfield1PlayerGame.height;

		spriteTextFieldOptions = new Sprite();		
		textFieldOptions = new TextField();	
		textFieldOptions.text = "Options";
		textFieldOptions.autoSize = TextFieldAutoSize.CENTER;
		textFieldOptions.setTextFormat( SharedDataGameTetris.Instance.textFormatWhite );
		
		spriteTextFieldOptions.addChild( textFieldOptions );
		textFieldOptions.height = (SharedDataGameTetris.Instance.textFormatWhite.size as uint) * 2;
		spriteTextFieldOptions.x = (  (SharedDataGameTetris.Instance.iAppWidth / 2) -
									  (textFieldOptions.width             / 2)    );
		spriteTextFieldOptions.y = uiYOffset;
		spriteTextFieldOptions.mouseChildren = false;
		spriteTextFieldOptions.buttonMode = true;
		
		uiYOffset += textfield1PlayerGame.height;

		spriteTextFieldHighScores = new Sprite();		
		textFieldHighScores = new TextField();	
		textFieldHighScores.text = "High Scores";
		textFieldHighScores.autoSize = TextFieldAutoSize.CENTER;
		textFieldHighScores.setTextFormat( SharedDataGameTetris.Instance.textFormatWhite );
		
		spriteTextFieldHighScores.addChild( textFieldHighScores );
		textFieldHighScores.height = (SharedDataGameTetris.Instance.textFormatWhite.size as uint) * 2;
		spriteTextFieldHighScores.x = (  (SharedDataGameTetris.Instance.iAppWidth / 2) -
									  	 (textFieldHighScores.width             / 2)    );
		spriteTextFieldHighScores.y = uiYOffset;
		spriteTextFieldHighScores.mouseChildren = false;
		spriteTextFieldHighScores.buttonMode = true;
				

		spriteTitle.filters                = [SharedDataGameTetris.Instance.dropShadowFilter];
		spriteTextField1PlayerGame.filters = [SharedDataGameTetris.Instance.dropShadowFilter];
		spriteTextField2PlayerGame.filters = [SharedDataGameTetris.Instance.dropShadowFilter];
		spriteTextFieldOptions.filters     = [SharedDataGameTetris.Instance.dropShadowFilter];
		spriteTextFieldHighScores.filters  = [SharedDataGameTetris.Instance.dropShadowFilter];
		
				
		this.addChild( spriteTitle );
		this.addChild( spriteTextField1PlayerGame );
		this.addChild( spriteTextField2PlayerGame );
		this.addChild( spriteTextFieldOptions );
		this.addChild( spriteTextFieldHighScores );		
		
	}
	
	//Take out
	public function Render():void
	{
																	
	}
	
	public function Enter():void
	{
				
		spriteTextField1PlayerGame.addEventListener( MouseEvent.MOUSE_OVER, SharedDataGameTetris.Instance.OnMenuItemMouseOver  );
		spriteTextField1PlayerGame.addEventListener( MouseEvent.MOUSE_OUT,  SharedDataGameTetris.Instance.OnMenuItemMouseOut   );
		spriteTextField1PlayerGame.addEventListener( MouseEvent.CLICK,      On1PlayerGameClick   );
		
		spriteTextField2PlayerGame.addEventListener( MouseEvent.MOUSE_OVER, SharedDataGameTetris.Instance.OnMenuItemMouseOver );
		spriteTextField2PlayerGame.addEventListener( MouseEvent.MOUSE_OUT,  SharedDataGameTetris.Instance.OnMenuItemMouseOut  );
		
		spriteTextFieldOptions.addEventListener( MouseEvent.MOUSE_OVER, SharedDataGameTetris.Instance.OnMenuItemMouseOver );
		spriteTextFieldOptions.addEventListener( MouseEvent.MOUSE_OUT,  SharedDataGameTetris.Instance.OnMenuItemMouseOut  );
		spriteTextFieldOptions.addEventListener( MouseEvent.CLICK,      OnOptionsClick      );
		
		spriteTextFieldHighScores.addEventListener( MouseEvent.MOUSE_OVER, SharedDataGameTetris.Instance.OnMenuItemMouseOver );
		spriteTextFieldHighScores.addEventListener( MouseEvent.MOUSE_OUT,  SharedDataGameTetris.Instance.OnMenuItemMouseOut  );	
		spriteTextFieldHighScores.addEventListener( MouseEvent.CLICK,      OnHighScoresClick   );	

		SharedDataGameTetris.Instance.spriteGameTetris.addChild( this );

	}
	
	public function Exit():void
	{				
		spriteTextField1PlayerGame.removeEventListener( MouseEvent.MOUSE_OVER, SharedDataGameTetris.Instance.OnMenuItemMouseOver  );
		spriteTextField1PlayerGame.removeEventListener( MouseEvent.MOUSE_OUT,  SharedDataGameTetris.Instance.OnMenuItemMouseOut   );
		spriteTextField1PlayerGame.removeEventListener( MouseEvent.CLICK,      On1PlayerGameClick   );
		
		spriteTextField2PlayerGame.removeEventListener( MouseEvent.MOUSE_OVER, SharedDataGameTetris.Instance.OnMenuItemMouseOver );
		spriteTextField2PlayerGame.removeEventListener( MouseEvent.MOUSE_OUT,  SharedDataGameTetris.Instance.OnMenuItemMouseOut  );
		
		spriteTextFieldOptions.removeEventListener( MouseEvent.MOUSE_OVER, SharedDataGameTetris.Instance.OnMenuItemMouseOver );
		spriteTextFieldOptions.removeEventListener( MouseEvent.MOUSE_OUT,  SharedDataGameTetris.Instance.OnMenuItemMouseOut  );
		spriteTextFieldOptions.removeEventListener( MouseEvent.CLICK,      OnOptionsClick      );
		
		spriteTextFieldHighScores.removeEventListener( MouseEvent.MOUSE_OVER, SharedDataGameTetris.Instance.OnMenuItemMouseOver );
		spriteTextFieldHighScores.removeEventListener( MouseEvent.MOUSE_OUT,  SharedDataGameTetris.Instance.OnMenuItemMouseOut  );	
		spriteTextFieldHighScores.removeEventListener( MouseEvent.CLICK,      OnHighScoresClick   );	

		SharedDataGameTetris.Instance.spriteGameTetris.removeChild( this );

	}
	
	public function Update( time:Number ):void
	{
	}
	
	public function addChildren():void
	{
	}
	
	public function removeChildren():void
	{
	}
	
	private function On1PlayerGameClick( mouseEvent:MouseEvent ):void
	{
		SharedDataGameTetris.Instance.stateMachine.ChangeState( SharedDataGameTetris.Instance.stateSpriteGameTetris1PlayerLoop );
		SharedDataGameTetris.Instance.OnMenuItemMouseOut( mouseEvent );
		
	}

	private function On2PlayerGameClick( mouseEvent:MouseEvent ):void
	{
	}
	
	private function OnOptionsClick( mouseEvent:MouseEvent ):void
	{
	}
	
	private function OnHighScoresClick( mouseEvent:MouseEvent ):void
	{
		SharedDataGameTetris.Instance.stateMachine.ChangeState( SharedDataGameTetris.Instance.stateSpriteGameTetrisHighScore );
		SharedDataGameTetris.Instance.OnMenuItemMouseOut( mouseEvent );
		
	}

	
}

class StateSpriteGameTetris1PlayerLoop extends Sprite implements IState
{
	private var spriteGameInfoPannel:SpriteGameInfoPannel;
	private var spritePlayPannel:SpritePlayPannel;
	
	public function StateSpriteGameTetris1PlayerLoop()
	{
		spriteGameInfoPannel = new SpriteGameInfoPannel();
		spritePlayPannel     = new SpritePlayPannel();
		
		this.addChild( spriteGameInfoPannel );
		this.addChild( spritePlayPannel );
		
	}
	
	public function Enter():void
	{
		Reset();
		
		SharedDataGameTetris.Instance.spriteGameTetris.addChild( this );
		/*
		refresh data
		
		*/
	}

	public function Initialize():void
	{
		spriteGameInfoPannel.Initialize();
		spritePlayPannel.Initialize();
		
		spritePlayPannel.x = SharedDataGameTetris.Instance.iAppWidth / 2;
		
	}
	
	public function Exit():void
	{
		SharedDataGameTetris.Instance.spriteGameTetris.removeChild( this );
		
	}	
	
	public function Update( time:Number ):void
	{
		
		if( spritePlayPannel.sharedDataPlayPannel.bIsTetradDispatched == false )
		{			
			spriteGameInfoPannel.NextTetrad = spritePlayPannel.sharedDataPlayPannel.iNextTetradType;
			spriteGameInfoPannel.UpdatePercentageBars();
			
		}
		
		if( spritePlayPannel.sharedDataPlayPannel.bIsGameOver == true )
		{
			spriteGameInfoPannel.GrayscaleBlocks();
			
		}

		spritePlayPannel.Update( time );
		
	}
	
	public function addChildren():void
	{
	}
	
	public function removeChildren():void
	{
	}
	
	public function Reset():void
	{
		spriteGameInfoPannel.Reset();
		spritePlayPannel.Reset();
	}
	
}

class StateSpriteGameTetris2PlayersLoop extends Sprite implements IState
{

	public function Enter():void
	{
	}
	
	public function Initialize():void
	{
	}
	
	public function Exit():void
	{
	}
	
	public function Update( time:Number ):void
	{
	}
	
	public function addChildren():void
	{
	}
	
	public function removeChildren():void
	{
	}
	
}

import flash.display.Sprite;
import flash.events.MouseEvent;
class StateSpriteGameTetrisHighScore extends Sprite implements IState
{
	private var spriteTextFieldHighScores:Sprite;
	
	public function StateSpriteGameTetrisHighScore()
	{
		spriteTextFieldHighScores = new Sprite();
		
	}
	
	public function Enter():void
	{
		//SharedDataGameTetris.Instance.spriteBackToMainMenu.addEventListener(MouseEvent.CLICK, SharedDataGameTetris.Instance.OnBackToMainMenuClick );
		addChildren();		
		
	}
	
	public function Initialize():void
	{

		var arrayFilters:Array;		
		var shapeBackground:Shape         = new Shape();
		var textFieldHighScores:TextField = new TextField();

		shapeBackground.graphics.beginFill( 0xffffff, 0.0 );
		shapeBackground.graphics.drawRect( 0,
										   0,
										   SharedDataGameTetris.Instance.iAppWidth,
										   SharedDataGameTetris.Instance.iAppHeight );
		shapeBackground.graphics.endFill( );
		this.addChild( shapeBackground );
		
		textFieldHighScores.text = "High Scores";
		textFieldHighScores.setTextFormat( SharedDataGameTetris.Instance.textFormatWhite );
		arrayFilters = textFieldHighScores.filters;
		arrayFilters.push( SharedDataGameTetris.Instance.dropShadowFilter );
		arrayFilters.push( SharedDataGameTetris.Instance.glowFilter       );
		
		textFieldHighScores.filters = arrayFilters;
		spriteTextFieldHighScores.addChild( textFieldHighScores );
		this.addChild( spriteTextFieldHighScores );
				
		spriteTextFieldHighScores.x = (  (SharedDataGameTetris.Instance.iAppWidth / 2) -
							             (spriteTextFieldHighScores.width / 2        )    );

	}
	
	public function Exit():void
	{
		//SharedDataGameTetris.Instance.spriteBackToMainMenu.removeEventListener(MouseEvent.CLICK, SharedDataGameTetris.Instance.OnBackToMainMenuClick );
		removeChildren();
		
	}
	
	public function Update( time:Number ):void
	{
		
	}
	
	public function addChildren():void
	{
		var textField:TextField;
		var iYOffset:int = ( spriteTextFieldHighScores.y + ((SharedDataGameTetris.Instance.textFormatWhite.size as int)*2) );

		SharedDataGameTetris.Instance.spriteTextFieldsHighScores.visible = false;
		
		
		for( var uiTextFieldIndex:uint = 1;
				 uiTextFieldIndex < SharedDataGameTetris.Instance.spriteTextFieldsHighScores.numChildren;
				 uiTextFieldIndex ++                                                                      )
		{
			textField = SharedDataGameTetris.Instance.spriteTextFieldsHighScores.getChildAt( uiTextFieldIndex ) as TextField;
			textField.setTextFormat( SharedDataGameTetris.Instance.textFormatWhite );
			
		}
		
		this.addChild( SharedDataGameTetris.Instance.spriteTextFieldsHighScores );
		SharedDataGameTetris.Instance.spriteTextFieldsHighScores.y = iYOffset;
	
		SharedDataGameTetris.Instance.spriteTextFieldsHighScores.visible = true;
		
		( SharedDataGameTetris.Instance.spriteBackToMainMenu.getChildAt(1) as TextField ).setTextFormat( SharedDataGameTetris.Instance.textFormatWhite );
		SharedDataGameTetris.Instance.spriteBackToMainMenu.visible = false;
		this.addChild( SharedDataGameTetris.Instance.spriteBackToMainMenu );
		SharedDataGameTetris.Instance.spriteBackToMainMenu.y = ( SharedDataGameTetris.Instance.iAppHeight ) - ( (SharedDataGameTetris.Instance.textFormatWhite.size as int) );
		SharedDataGameTetris.Instance.spriteBackToMainMenu.visible = true;
		
		SharedDataGameTetris.Instance.spriteGameTetris.addChild( this );
		
	}
	
	public function removeChildren():void
	{
		this.removeChild( SharedDataGameTetris.Instance.spriteTextFieldsHighScores );
		this.removeChild( SharedDataGameTetris.Instance.spriteBackToMainMenu );

		SharedDataGameTetris.Instance.spriteGameTetris.removeChild( this );
		
	}
	

}


//////
class TetradType
{
	public var arrayPointBlocks:Array;

	public var functionRotateCW:Function;
	public var functionRotateCC:Function;

	
	public function TetradType(  pointblock2:Point,
								 pointblock3:Point,
								 pointblock4:Point,
								 functionrotateCW:Function,
								 functionrotateCC:Function  )
								    
	{
		arrayPointBlocks = new Array();
		
		arrayPointBlocks.push( pointblock2 );
		arrayPointBlocks.push( pointblock3 );
		arrayPointBlocks.push( pointblock4 );
		
		functionRotateCW = functionrotateCW;
		functionRotateCC = functionrotateCC;
								    
	}
	
	public function RotateCW( p1:Point, p2:Point, p3:Point, p4:Point ):void
	{
		functionRotateCW( p1, p2, p3, p4 );
	}
		
	public function RotateCC( p1:Point, p2:Point, p3:Point, p4:Point ):void
	{
		functionRotateCC( p1, p2, p3, p4 );		
	}
	
	public static function FunctionRotateCW( p1:Point, p2:Point, p3:Point, p4:Point ):void
	{
	}
	
	public static function FunctionRotateCC( p1:Point, p2:Point, p3:Point, p4:Point ):void
	{
	}
	
	public static function FunctionRotateCW2by2Block( p1:Point, p2:Point, p3:Point, p4:Point ):void
	{
	}
	
	public static function FunctionRotateCC2by2Block( p1:Point, p2:Point, p3:Point, p4:Point ):void
	{
	}

	
}

class BlockPile extends Sprite
{
	private var arrayBlocks:Array;
	
	
}

import flash.display.Sprite;
import flash.events.Event;
import flash.display.Shape;
import tetris.SpriteGameTetris;
import flash.geom.Rectangle;
import flash.geom.ColorTransform;
import flash.geom.Matrix;
import flash.filters.ColorMatrixFilter;
import flash.events.MouseEvent;
import flash.display.DisplayObjectContainer;
import flash.utils.getTimer;
import flash.text.TextFormat;
import flash.text.TextField;
import flash.filters.DropShadowFilter;
import flash.filters.GlowFilter;
import mx.messaging.AbstractConsumer;

class Block extends Sprite
{
	public static var spriteGameTetris:SpriteGameTetris;

	public var  Active:Boolean;	  //used?
	public var  arrayColorMatrix:Array;
	public var  bIsGrayscale:Boolean;
	
	public var  colorMatrixFilter:ColorMatrixFilter;
	
	public var  uiColorPrev:uint;
	public var  uiColorNext:uint;
	
	private var numberTimeStart:Number;
    private var numberTimeElapsed:Number;
    private var numberTimeFrame:Number;
	private var numberTimePeriod:Number = 3500;
	
	private var intData:int;
	private var shapeTopLayer:Shape;
	private var shapeBottomLayer:Shape;
	
	//private var rectangle:Rectangle;
		
	//grid 20(24) * 10
	 //16 8 4 2 1  32
		//8421      16
	public function Block( iWidth:int, iHeight:int )
	{	
		bIsGrayscale = false;
		numberTimeStart = getTimer();
		numberTimeFrame = getTimer();
		
		uiColorPrev = GenerateRandomColor();
		uiColorNext = GenerateRandomColor();
		var nColorRed:Number   = (uiColorPrev >> 16 & 0xff)/256;
		var nColorGreen:Number = (uiColorPrev >>  8 & 0xff)/256;
		var nColorBlue:Number  = (uiColorPrev >>  0 & 0xff)/256;
		
		Active = true;
		
		arrayColorMatrix = new Array();
		arrayColorMatrix = arrayColorMatrix.concat([nColorRed,           0,          0,         0,        0]); // red
        arrayColorMatrix = arrayColorMatrix.concat([        0, nColorGreen,          0,         0,        0]); // green
        arrayColorMatrix = arrayColorMatrix.concat([        0,           0, nColorBlue,         0,        0]); // blue
        arrayColorMatrix = arrayColorMatrix.concat([        0,           0,          0,        .5,        0]); // alpha
        
        colorMatrixFilter = new ColorMatrixFilter(arrayColorMatrix);
        
        var arrayFilters:Array = new Array();
        arrayFilters.push(colorMatrixFilter);
  
		shapeTopLayer = new Shape();
		shapeTopLayer.graphics.lineStyle( 1, 0xffffff, 1 );
		shapeTopLayer.graphics.moveTo(0, 0);
		shapeTopLayer.graphics.lineTo(0, iHeight);
		shapeTopLayer.graphics.lineTo(iWidth, iHeight);	
		shapeTopLayer.graphics.lineTo(iWidth, 0);
		shapeTopLayer.graphics.lineTo(0, 0);
		
		shapeBottomLayer = new Shape();
		shapeBottomLayer.filters = arrayFilters;
		
		shapeBottomLayer.graphics.beginFill( 0xffffff, 1.0 );
		shapeBottomLayer.graphics.drawRect(0, 0, iWidth, iHeight);
		shapeBottomLayer.graphics.endFill( );

		this.addChild( shapeBottomLayer );
		this.addChild( shapeTopLayer );
		
		addEventListener( Event.ENTER_FRAME, OnBlockEnterFrame );
		
	}
	
	public function get Row():int
	{
		return ((intData & 0xff0) >> 4);
		
	}

	public function set Row( iRow:int ):void
	{
		intData = (intData & 0xfffff00f) | (iRow << 4);
		
	}
	
	public function get Column():int
	{
		return (intData & 0xf);		
		
	}
		
	public function set Column( iCol:int ):void
	{
		intData = (intData & 0xfffffff0) | iCol;
	}
	
	private function OnBlockEnterFrame( ev:Event ):void
	{
		var uiColorCurr:uint;
		var numberPercentage:Number;
		
		numberTimeElapsed = getTimer() - numberTimeStart;
		numberPercentage = numberTimeElapsed / numberTimePeriod;
		
		if( numberPercentage > 1.00 )
        {
        	numberPercentage = 0;
                	
            numberTimeStart = getTimer();
                	
            uiColorPrev = uiColorNext;
            if( bIsGrayscale )
            {
            	uiColorNext = GenerateRandomGrayscale();
            	
            }
            else
            {
            	uiColorNext = GenerateRandomColor();
            	
            }
            
        }
        
        uiColorCurr = Utilities.MergeColors( uiColorPrev, uiColorNext, numberPercentage);
		
		var arrayFilters:Array = shapeBottomLayer.filters;

		arrayColorMatrix[0]  = ( (uiColorCurr >> 16) & 0xff ) / 256.00;
		arrayColorMatrix[6]  = ( (uiColorCurr >>  8) & 0xff ) / 256.00;
		arrayColorMatrix[12] = ( (uiColorCurr >>  0) & 0xff ) / 256.00;
		
		(arrayFilters[0] as ColorMatrixFilter).matrix = arrayColorMatrix;

		shapeBottomLayer.filters = arrayFilters;

	}
	
		
}

internal function MergeColors( iColor1:int, iColor2:int, number2ndColorPercent:Number ):int
{			
	var intDeltaR:int;
    var intDeltaG:int;
    var intDeltaB:int;
    
    var intNewR:int;
    var intNewG:int;
    var intNewB:int;
    
    var iColorReturn:int;
    
	if( number2ndColorPercent < 0.00 )
	{
		number2ndColorPercent = 0.00;
	}
	else if( number2ndColorPercent > 1.00 )
	{
		number2ndColorPercent = 1.00;							
	}
	
	intDeltaR = ( (iColor2 >> 16) & 0xFF ) - ( (iColor1 >> 16) & 0xFF );
    intDeltaG = ( (iColor2 >> 8 ) & 0xFF ) - ( (iColor1 >> 8 ) & 0xFF );
    intDeltaB = ( (iColor2      ) & 0xFF ) - ( (iColor1      ) & 0xFF );
        	
    intNewR   = (  ( (iColor1 >> 16) & 0xFF )  ) + Math.floor( intDeltaR * number2ndColorPercent );
    intNewG   = (  ( (iColor1 >> 8 ) & 0xFF )  ) + Math.floor( intDeltaG * number2ndColorPercent );
    intNewB   = (  ( (iColor1      ) & 0xFF )  ) + Math.floor( intDeltaB * number2ndColorPercent );
        	
    iColorReturn = (  ( (intNewR << 16) & 0xFF0000 ) |
        			  ( (intNewG << 08) & 0x00FF00 ) | 
        			  ( (intNewB      ) & 0x0000FF )    ); 
	
	return iColorReturn;

} 

internal function GenerateRandomGrayscale():int
{
	var intRandomShade:int = Math.round(Math.random()*0xff);
	
	intRandomShade += (intRandomShade << 8) + (intRandomShade << 16);
                
    return (  Math.min(0xFFFFFF, intRandomShade) );    
	
}
 
internal function GenerateRandomColor():int
{
	var intRandomShade:int = Math.round(Math.random()*0xffffff);                
                
    return (  Math.min(0xFFFFFF, intRandomShade) );
    
}
