package
{
	import flash.events.Event;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.media.SoundMixer;
	import flash.media.SoundTransform;
	import flash.net.URLRequest;
	
	public class SoundManager
	{
		//public var arrSoundChannelsActive:Array; //HANDLED WITH THE EVENT LISTENER
		public var arrSoundChannelsIdle:Array;
		public var arrSounds:Array;
		public var soundMixer:SoundMixer;
		public var soundPlaceholder:Sound;
		public var soundchannelPlaceholder:SoundChannel;
		public var soundtransformPlaceholder:SoundTransform;
		
		
		public function SoundManager( uiSoundChannelQty:uint = 32 )
		{
			arrSoundChannelsIdle = new Array();
			arrSounds            = new Array();
		
			if( uiSoundChannelQty > 32 ||
			    uiSoundChannelQty < 1     )
			{
				uiSoundChannelQty = 32;
			}
			
			for( var uiSoundChannelIndex:uint = 0;
					 uiSoundChannelIndex < uiSoundChannelQty;
					 uiSoundChannelIndex ++                   )
			{
				soundchannelPlaceholder = new SoundChannel();
				arrSoundChannelsIdle.push( soundchannelPlaceholder );
			}
						
		}

		public function Play( uiSoundIndex:int ):SoundChannel
		{			
			if( ( uiSoundIndex > arrSounds.length     ) &&
				( uiSoundIndex < 1                    ) &&
				( arrSounds[ uiSoundIndex-1 ] == null ) && 
			    ( arrSoundChannelsIdle.length == 0    )    )
				return null;				
	
			soundchannelPlaceholder = ( arrSoundChannelsIdle.pop() as SoundChannel );
			soundchannelPlaceholder = ( arrSounds[ uiSoundIndex-1 ] as Sound ).play();
			soundchannelPlaceholder.addEventListener( Event.SOUND_COMPLETE, OnSoundPlayOnceComplete );
			
			return soundchannelPlaceholder;
			
		}
		
		public function PlayLooped( uiSoundIndex:uint ):SoundChannel
		{			
			if( ( arrSounds.length            <= uiSoundIndex ) &&
				( arrSounds[ uiSoundIndex-1 ] == null         ) && 
			    ( arrSoundChannelsIdle.length == 0            )    )
				return null;				
	
			soundchannelPlaceholder = ( arrSoundChannelsIdle.pop() as SoundChannel );
			soundchannelPlaceholder.addEventListener( Event.SOUND_COMPLETE, OnSoundLoopedPlayComplete );
			soundchannelPlaceholder = ( arrSounds[ uiSoundIndex-1 ] as Sound ).play();
			
			return soundchannelPlaceholder;
			
		}
	
/* lstChannelPlaying	
		public function Stop( uiChannelIndex:uint = 0 ):void
		{
			if( uiChannelIndex == 0 )
			{
				for( uiChannelIndex = 0;
					 uiChannelIndex < lstChannelPlaying.length;
					 uiChannelIndex ++                          )
				{
					(lstChannelPlaying[ iChannelIndex ] as SoundChannel).stop();
				}				
			}
			else
			{
				if(  ( iChannelIndex < lstChannelPlaying.length     ) &&
					 ( lstChannelPlaying[ iChannelIndex-1 ] != null )     )
				{
					(lstChannelPlaying[ iChannelIndex-1 ] as SoundChannel).stop();
				}
			}
	
		}

	
		public function Volume( nVolume:Number, uiChannelIndex:uint = 0 ):Boolean
		{
			
			if( uiChannelIndex == 0 )
			{
				for( var uiChannelIndex = 0;
					     uiChannelIndex < lstChannelPlaying.length;
					     uiChannelIndex ++                         )
				{
					soundtransformPlaceholder = (lstChannelPlaying[ iChannelIndex ] as SoundChannel).soundTransform;
					soundtransformPlaceholder.volume = nVolume;
					(lstChannelPlaying[ iChannelIndex ] as SoundChannel).soundTransform = soundtransformPlaceholder;
				}
				
				return true;
			}
			else
			{
				if(  ( iChannelIndex < lstChannelPlaying.length     ) &&
					 ( lstChannelPlaying[ iChannelIndex-1 ] != null )     )
				{
					soundtransformPlaceholder = (lstChannelPlaying[ iChannelIndex-1 ] as SoundChannel).soundTransform;
					soundtransformPlaceholder.volume = nVolume;
					(lstChannelPlaying[ iChannelIndex-1 ] as SoundChannel).soundTransform = soundtransformPlaceholder;
				}
			}
			
		}

		public function Panning( nPanning:Number, iChannelIndex:int = 0 ):void
		{
			if( iChannelIndex == 0 )
			{
				for( var iChannelIndex = 0;
					 iChannelIndex < lstChannelPlaying.length;
					 iChannelIndex ++                         )
				{
					soundtransformPlaceholder = (lstChannelPlaying[ iChannelIndex ] as SoundChannel).soundTransform;
					soundtransformPlaceholder.pan = nPanning;
					(lstChannelPlaying[ iChannelIndex ] as SoundChannel).soundTransform = soundtransformPlaceholder;
				}
				
				return true;
			}
			else
			{
				if( (iChannelIndex - 1) <= lstChannelPlaying.length )
				{
					soundtransformPlaceholder = (lstChannelPlaying[ iChannelIndex-1 ] as SoundChannel).soundTransform;
					soundtransformPlaceholder.pan = nPanning;
					(lstChannelPlaying[ iChannelIndex-1 ] as SoundChannel).soundTransform = soundtransformPlaceholder;
				}
			}
			
		}


		public function Mute( bMute:Boolean, iChannelIndex:int = 0 ):void;
		{
			if( iChannelIndex == 0 )
			{
				for( var iChannelIndex = 0;
					 iChannelIndex < lstChannelPlaying.length
					 iChannelIndex ++                         )
				{
					channelPlaceholder = lstChannelPlaying[iChannelIndex];
					channelPlaceholder.Panning( nPanning);
				}
				
				return true;
			}
			else
			{
				if( (iChannelIndex - 1) <= lstChannelPlaying.length )
				{
					lstChannelPlaying[iChannelIndex].nPanning = nPanning
				}
			}
		}
	
		public function Pause( iChannelIndex:int = 0 ):void;
		public function FadeIn( nTime:Number, iChannelIndex:int = 0 ):void;
		public function FadeOut( nTime:Number, iChannelIndex:int = 0 ):void;

*/		
		
		public function Load( strFilenameURL:String ):uint
		{
			var uiReturnedSoundIndex:uint = 1;
			var urlrequest:URLRequest = new URLRequest( strFilenameURL );

			try
			{
				soundPlaceholder = new Sound();
	            soundPlaceholder.load(urlrequest);
            }
            catch(error:Error)
            {
                uiReturnedSoundIndex = 0;
            }
			
			if( uiReturnedSoundIndex == 1 )			
			{
				/* Unload later on*/
				arrSounds.push( soundPlaceholder );
				uiReturnedSoundIndex = arrSounds.length;
			}
			
			return uiReturnedSoundIndex;
			
		}
		
		private function OnSoundPlayOnceComplete( event:Event ):void
		{			
			arrSoundChannelsIdle.push( event.target as SoundChannel );
			( event.target as SoundChannel ).removeEventListener( Event.SOUND_COMPLETE, OnSoundPlayOnceComplete );
			
		}
		
		private function OnSoundLoopedPlayComplete( event:Event ):void
		{
			/*todo - soundChannel class overload that stores the location of volume for the mute and pause and the */
			/*( event.target() as SoundChannel ).position = 0; */
			
		}

	}
}