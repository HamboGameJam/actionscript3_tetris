package
{
	import flash.display.DisplayObject;
	import flash.events.MouseEvent;
	import flash.filters.DropShadowFilter;
	import flash.system.*;
	
	import mx.containers.HBox;
	import mx.controls.Label;
	import mx.core.Application;
	
	public class SharedData
	{
		public var labelMenu0:Label;
		public var labelMenu1:Label;
		public var labelMenu2:Label;
		public var labelMenu3:Label;
		public var labelMenu4:Label;		
		public var labelMiddletop:Label;
		
		public var statemachine:StateMachine;
		public var stateMainMenu:IState;
		public var stateDemoReel:IState;
		public var stateGameTetris:IState;
				
		public var soundManager:SoundManager;
//		public var uiSoundThemeSong:uint;
//		public var uiSoundMachineGun:uint;

		public var intBGColorCurr:int;
        public var intBGColorNext:int;
        public var intBGColorPrev:int;
				
		public var hboxMainpannel:HBox;		
		public var application:Application;
		
		public var shadowFilter:DropShadowFilter;

										
		static private var instance:SharedData = null;
		
		public function SharedData()
		{
			intBGColorCurr    = 0x888888;
			
			statemachine      = new StateMachine();
          	stateMainMenu     = new StateMainMenu();
          	stateDemoReel     = new StateDemoReel(); 
          	stateGameTetris   = new StateGameTetris();
          	soundManager      = new SoundManager();
          	
          	shadowFilter = new DropShadowFilter();
            shadowFilter.color = 0x000000;
            shadowFilter.alpha = 0.4;
            shadowFilter.blurX = 5;
            shadowFilter.blurY = 5;
            shadowFilter.distance = 5;            
                        
		}
		            
		public function addShadow(comp:DisplayObject):void
        {
            comp.filters = [this.shadowFilter];
            
        }
        
		
		public static function get Instance():SharedData
		{
			if( instance == null )
			{
				instance = new SharedData();
			}
			
           	return instance;
           	
		}
		
		public function onClickMainMenu(evt:MouseEvent):void
        {
    		statemachine.ChangeState( stateMainMenu );
        }
        
		public function onClickExit(evt:MouseEvent):void
        {
           application.stage.nativeWindow.close();
           
        }

	}
}